'use strict';

/* Authentication services */

var authenticationServices = angular.module('authenticationServices', ['ngResource']);

authenticationServices.factory('authorizationByToken', ['$resource',
    function($resource){
        return $resource(
            '/api/authorization/authenticate',
            { },
            { query: { method:'GET' } }
        );
    }]);

authenticationServices.factory('authorization', ['$resource',
    function($resource){
        return $resource(
            '/api/authorization/authenticate',
            {
                username:'@username',
                password:'@password',
                rememberMe:'@rememberMe'
            },
            { query: { method:'GET' } }
        );
    }]);