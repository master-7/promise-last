'use strict';

/* Profile services */

var profileServices = angular.module('profileServices', ['ngResource']);

profileServices.factory('updateProfile', ['$http',
    function($http) {
        return {
            query: function(data) {
                return $http({
                    url: '/api/rest/updateprofile',
                    method: 'POST',
                    data: JSON.stringify(data),
                    headers: {'Content-Type': 'application/json;charset=utf-8'}
                })
            }
        }
    }]);

profileServices.factory('registration', ['$http',
    function($http) {
        return {
            query: function(data) {
                return $http({
                    url: '/api/authorization/registration',
                    method: 'POST',
                    data: JSON.stringify(data),
                    headers: {'Content-Type': 'application/json;charset=utf-8'}
                })
            }
        }
    }]);