/**
 * Main application controllers
 */

'use strict';

var appCtrl = angular.module('appCtrl', ['ngMaterial', 'ngMessages', 'ngCookies']);

/**
 * Main app controller. Charge for the views application and do a broadcast
 */
appCtrl.controller('mainPageCtrl', ['$rootScope', '$scope', '$mdSidenav', '$mdDialog',
    '$location', '$cookies', 'authorizationByToken',
    function($rootScope, $scope, $mdSidenav, $mdDialog,
             $location, $cookies, authorizationByToken) {

        /**
         * Application user
         * @type {null}
         */
        $rootScope.user = { };

        /**
         * Toggle the sidenav
         * @param menuId
         */
        $scope.toggleSidenav = function(menuId) {
            $mdSidenav(menuId).toggle();
        };

        /**
         * Show the entrance form
         * @param ev
         */
        $scope.showEntranceForm = function(ev) {
            $mdDialog.show({
                controller: 'entranceCtrl',
                templateUrl: '/views/dialogs/entrance.html',
                parent: angular.element(document.body),
                targetEvent: ev,
            });
        };

        /**
         * User logout event handler
         * @param ev
         */
        $scope.exit = function(ev) {
            $cookies.remove('authKey');
            $rootScope.user = null;
            window.location.assign('/api/authorization/logout');
        };

        /**
         * Broadcast events
         */

        /**
         * This a event loadMore if bottom is a achieved
         */
        $scope.loadMore = function() {
            $scope.$broadcast('loadMore', $scope.toChild);
        };

        /**
         * Catch and handling exceptions http
         * @param response
         */
        $scope.catchHttpErrors = function(response) {
            if(response.status == 404) {
                $location.path("/404");
                $scope.$apply();
                return 404;
            }
            if(response.status == 403) {
                console.log('User is not authorize!');
                return 403;
            }
        };

        /**
         * Try authorize the user
         */
        authorizationByToken.query(
            function(data) {
                $rootScope.user = data;

                var conn = new WebSocket('ws://localhost:8080');

                conn.onopen = function(e) {
                    conn.send($rootScope.user.username);
                };

                conn.onmessage = function(e) {
                    console.log(e.data);
                };
            },
            function(response) {
                $scope.catchHttpErrors(response);
            }
        );
    }]);

/**
 * The entrance controller - authenticate user
 */
appCtrl.controller('entranceCtrl', ['$rootScope', '$scope', '$mdDialog', '$timeout', 'authorization',
    function($rootScope, $scope, $mdDialog, $timeout, authorization) {

        /**
         * Validator of a login or password
         * @type boolean
         */
        $scope.wrongLoginOrPass = false;
        $scope.countAttemptMax = false;
        $scope.query = false;

        /**
         * The email pattern for validation
         * @type {RegExp}
         */
        $scope.emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        /**
         * The user data for validation
         * @type {{username: null, password: null, rememberMe: boolean}}
         */
        $scope.user = {
            username: null,
            password: null,
            rememberMe: false
        };

        /**
         * Close the dialog of a authentication
         */
        $scope.close = function() {
            $mdDialog.hide();
        };

        /**
         * Entrance action
         * If good - setup $rootScope.user
         * If bad - set md-warn to form
         */
        $scope.entrance = function() {
            $scope.query = true;
            authorization.query({
                username: $scope.user.username,
                password: $scope.user.password,
                rememberMe: $scope.user.rememberMe
            }).$promise.then(
                function(data) {
                    $scope.wrongLoginOrPass = false;
                    $rootScope.user = data;
                },
                function(response) {
                    if(response.status == 403) {
                        $scope.query = false;
                        $scope.user.password = undefined;
                        var errors = JSON.parse($(response.data).find('.container .site-error .alert.alert-danger').text());
                        for(var key in errors) {
                            switch (key) {
                                case 'countFailAttempt':
                                    $scope.wrongLoginOrPass = false;
                                    $scope.countAttemptMax = true;
                                    $timeout(function() { $mdDialog.hide() }, 5000);
                                    break;
                                case 'password':
                                    $scope.wrongLoginOrPass = true;
                                    $scope.countAttemptMax = false;
                                    break;
                                default :
                                    $scope.wrongLoginOrPass = false;
                                    $scope.countAttemptMax = false;
                                    break;
                            }
                        }
                    }
                }
            );
        };
    }]);

/**
 * Profile controller allow users update in own profile
 */
appCtrl.controller('profileCtrl', ['$scope', '$mdDialog', 'authorization',
    function($scope, $mdDialog, authorization) {

        /**
         * Origin profile object
         * @type {{}}
         */
        var profileOrigin = { };

        /**
         * Base profile object
         * Copied JSON.stringify response data authorization
         * @type {null}
         */
        $scope.profile = null;

        /**
         * The email pattern for validation
         * @type {RegExp}
         */
        $scope.emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        /**
         * Condition the change form
         * @type {boolean}
         */
        $scope.formNotChanged = true;

        /**
         * Watch profile object - if change data - unlock form
         */
        $scope.$watch('profile', function(newValue, oldValue) {
            $scope.formNotChanged = JSON.stringify(newValue) == profileOrigin;
        }, true);

        /**
         * Get profile data for the authentication user
         */
        authorization.query().$promise.then(
            function(data) {
                $scope.profile = data;
                if($scope.profile.avatar != null)
                    $scope.profile.avatar = '/images/avatar/' + $scope.profile.avatar;

                profileOrigin = JSON.stringify($scope.profile);
            }
        );

        /**
         * Before update action
         * Dialog the asks enter the password
         * @param ev
         */
        $scope.update = function(ev) {
            $mdDialog.show({
                controller: 'updateProfileCtrl',
                templateUrl: '/views/dialogs/confirmation.html',
                parent: angular.element(document.body),
                locals: {
                    profile: $scope.profile
                },
                targetEvent: ev
            });
        };
    }]);

/**
 * The entrance controller - authenticate user
 */
appCtrl.controller('updateProfileCtrl', ['$scope', '$mdDialog', '$timeout', 'profile', 'authorization', 'updateProfile',
    function($scope, $mdDialog, $timeout, profile, authorization, updateProfile) {

        /**
         * User password
         * @type {{password: null}}
         */
        $scope.user = {
            checkPass: null,
            avatar: profile.avatar,
            username: profile.username,
            name: profile.name,
            surname: profile.surname,
            sex: profile.sex,
            password: profile.password
        };

        /**
         * Wrong pass validation
         * @type {boolean}
         */
        $scope.wrongPass = false;
        $scope.countAttemptMax = false;
        $scope.query = false;


        /**
         * Close the dialog of a authentication
         */
        $scope.close = function() {
            $mdDialog.hide();
        };

        /**
         * If query is success close the dialog
         * @param event
         */
        $scope.confirm = function(event) {
            $scope.query = true;
            updateProfile.query($scope.user).then(
                function (data) {
                    $scope.wrongPass = false;
                    $mdDialog.hide();
                },
                function (response) {
                    if(response.status == 403) {
                        $scope.query = false;
                        $scope.user.checkPass = undefined;
                        var errors = JSON.parse($(response.data).find('.container .site-error .alert.alert-danger').text());
                        console.log(response.data);
                        console.log(errors);
                        for(var key in errors) {
                            switch (key) {
                                case 'countFailAttempt':
                                    $scope.wrongPass = false;
                                    $scope.countAttemptMax = true;
                                    $timeout(function() { $mdDialog.hide() }, 5000);
                                    break;
                                case 'password':
                                    $scope.wrongPass = true;
                                    $scope.countAttemptMax = false;
                                    break;
                                default :
                                    $scope.wrongPass = false;
                                    $scope.countAttemptMax = false;
                                    break;
                            }
                        }
                    }
                }
            );
        };
    }]);

/**
 * The entrance controller - authenticate user
 */
appCtrl.controller('registrationCtrl', ['$rootScope', '$scope', '$location', 'registration',
    function($rootScope, $scope, $location, registration) {

        /**
         * The email pattern for validation
         * @type {RegExp}
         */
        $scope.emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        /**
         * Base profile object
         * Copied JSON.stringify response data authorization
         * @type {null}
         */
        $scope.userForm = { };

        /**
         * Set the user form validity
         */
        $scope.$watch('userForm', function() {
            $scope.userForm.username.$setValidity("exist", true);
        });

        /**
         * Send registration data
         */
        $scope.registration = function() {
            registration.query({
                avatar: $scope.user.avatar,
                name: $scope.user.name,
                surname: $scope.user.surname,
                sex: $scope.user.sex,
                username: $scope.user.username,
                password: $scope.user.password
            }).then(
                function(data) {
                    for(var key in data.data) {
                        if(key == 'username') {
                            $scope.userForm.username.$setValidity("exist", false);
                            return false;
                        }
                    }
                    $scope.userForm.username.$setValidity("exist", true);

                    $rootScope.user = data;
                    //Reload page
                    window.location.assign('/home');
                },
                function(response) {
                    $scope.userForm.username.$setValidity("exist", false);
                }
            );
        };
    }]);