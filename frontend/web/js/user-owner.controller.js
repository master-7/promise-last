/**
 * Authenticate user actions.
 * Return event and promises where user is a owner
 */

'use strict';

/**
 * Promises controller for authenticate user
 */
application.registerCtrl('myPromisesCtrl', ['$scope', 'ownPromises',
    function($scope, ownPromises) {
        $scope.promises = [];
        ownPromises.query(
            function(data) {
                for(var key in data) {
                    $scope.promises.push(JSON.parse(data[key]));
                }
                console.log($scope.promises);
            },
            function(response) {
                console.log(response);
            }
        )
    }]);

/**
 * Promise controller for authenticate user
 */
application.registerCtrl('myPromiseCtrl', ['$scope', '$routeParams', 'ownPromises',
    function($scope, $routeParams, ownPromises) {
        $scope.promises = null;
        ownPromises.query(
            function(data) {
                $scope.promises = data;
            },
            function(response) {
                console.log(response);
            }
        )
    }]);

/**
 * Add promise controller for authenticate user
 */
application.registerCtrl('addPromiseCtrl',
    ['$rootScope', '$scope', '$routeParams', '$location', '$mdDialog', 'enums', 'ownPromises',
    function($rootScope, $scope, $routeParams, $location, $mdDialog, enums, ownPromises) {
        $scope.typeRate = enums.typeRate;

        $scope.currentDate = new Date();

        $scope.promise = {
            image: null,
            title: null,
            category_id: null,
            short_text: null,
            description: null,
            deadline: null,
            typeRate: null,
            rate: null
        };

        $scope.addPromise = function(ev) {
            ownPromises.save($scope.promise,
                function (data) {
                    if(data[0] == 1) {
                        $location.path('/my-promise');
                    }
                    else {
                        $mdDialog.show({
                            controller: 'errorReportsCtrl',
                            templateUrl: '/views/dialogs/error-validate.html',
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose:true
                        })
                    }
                },
                function (response) {
                    if(response.status == 500) {
                        $mdDialog.show({
                            controller: 'errorReportsCtrl',
                            templateUrl: '/views/dialogs/error.html',
                            parent: angular.element(document.body),
                            locals: {
                                data: response
                            },
                            targetEvent: ev
                        });
                    }
                }
            );
        }
    }]);

/**
 * Add promise work controller for authenticate user
 */
application.registerCtrl('addPromiseWorkCtrl', ['$scope', '$routeParams', '$sce', '$location', 'enums', 'workPromise',
    function($scope, $routeParams, $sce, $location, enums, workPromise) {

        /**
         * Promise work object
         * @type {{promise_id: *, logo: null, title: null, description: null, gallery: {photos: Array, videos: Array}}}
         */
        $scope.promiseWork = {
            promise_id: $routeParams.promise_id,
            logo: null,
            title: null,
            description: null,
            gallery: {
                photos: [],
                videos: []
            }
        };

        /**
         * Delete photo in photo list
         * @param index
         */
        $scope.deletePhoto = function (index) {
            delete $scope.promiseWork.gallery.photos[index];
        };

        //Video variable
        $scope.currentVideo = "";
        $scope.videoLink = "";
        $scope.showVideo = "";
        $scope.videos = [];

        //Add video preview across in a watch variable
        $scope.$watch("currentVideo", function(newValue, oldValue) {
            //Check video hosting - if exists - add video
            for(var key in enums.videoHosting) {
                var host = enums.videoHosting[key];
                if(host.pregTest) {
                    //If current video is a know video host - make true link
                    if (host.pregTest.test($scope.currentVideo)) {
                        $scope.videoValid = enums.videoStatus.valid;
                        var videoId = $scope.currentVideo.match(host.pregMatch)[1];
                        $scope.videoLink = host.link.replace(/:link$/, videoId);
                        break;
                    }
                    $scope.videoValid = enums.videoStatus.notValid;
                }
            }
            if($scope.videoValid == enums.videoStatus.valid) {
                $scope.showVideo = $sce.trustAsResourceUrl($scope.videoLink);
            }
            else {
                $scope.currentVideo = "";
                $scope.showVideo = "";
            }
        });

        /**
         * Add video to video list
         * @param event
         */
        $scope.addVideo = function (event) {
            if($scope.showVideo) {
                $scope.promiseWork.gallery.videos.push($scope.videoLink);
                $scope.videos.push($scope.showVideo);
                $scope.videoLink = null;
                $scope.currentVideo = null;
                $scope.showVideo = null;
            }
        };

        /**
         * Delete video for video list
         * @param index
         */
        $scope.deleteVideo = function (index) {
            delete $scope.promiseWork.gallery.videos[index];
        };

        /**
         * Add the promise work
         */
        $scope.addPromiseWork = function () {
            workPromise.save($scope.promiseWork,
                function (data) {
                    $location.path("/promise/"+$routeParams.promise_id);
                    $scope.$apply();
                },
                function (response) {
                    if(response.status == 403) {
                        $location.path("/403-forbidden");
                        $scope.$apply();
                        return 403;
                    }
                }
            );
        }
    }]);

/**
 * Promise controller for authenticate user
 */
application.registerCtrl('myPromiseCtrl', ['$scope', '$routeParams', 'ownPromises',
    function($scope, $routeParams, ownPromises) {
        $scope.promises = null;
        ownPromises.query(
            function(data) {
                $scope.promises = data;
            },
            function(response) {

            }
        )
    }]);


