'use strict';

/**
 * User owner services
 */
var userOwnerServices = angular.module('userOwnerServices', ['ngResource']);

/**
 * User promises save and get
 */
userOwnerServices.factory('ownPromises', ['$resource',
    function($resource){
            return $resource('/api/userowner/promises', {}, {
            query: {method:'GET', isArray:true}
        });
    }]);

/**
 * User work promises save and get
 */
userOwnerServices.factory('workPromise', ['$resource',
    function($resource){
            return $resource('/api/userowner/workpromise', {}, {
            query: {method:'GET', isArray:true}
        });
    }]);