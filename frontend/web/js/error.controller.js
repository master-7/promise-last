/**
 * Error reports controller.
 * Make reports about error
 */

'use strict';

var errorCtrl = angular.module('errorCtrl', ['ngMaterial', 'ngMessages']);

/**
 * Error reports controller
 */
errorCtrl.controller('errorReportsCtrl', ['$scope', '$mdDialog', 'error', 'data',
    function($scope, $mdDialog, error , data) {

        $scope.errors = {
            comment: null,
            value: data
        };

        $scope.hide = function(event) {
            $mdDialog.hide();
        };

        $scope.sendError = function (ev) {
            error.save($scope.errors);
            $mdDialog.hide();
        }
    }]);