'use strict';

/* Directive Module */

var appDirective = angular.module('appDirective', []);

/**
 * Directive infinity scroll
 */
appDirective.directive('whenScrolled', function() {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			var raw = element[0];

			var funCheckBounds = function(evt) {
				if (raw.scrollHeight <= (raw.scrollTop + 1000)) {
					scope.loadMore();
				}
			};

			element.bind('scroll load', funCheckBounds);
		}
	};
});

/**
 * Comment user avatar directive.
 */
appDirective.directive('imgAvatar', function($compile) {
    return {
        restrict: 'E',
        replace: true,
        link: function (scope, element, attrs) {
            if(attrs.src) {
                element[0].outerHTML = '<img ng-show="' + attrs.ngshow + '" src="/images/avatar/' + attrs.src + '" class="md-avatar ' + attrs.class + '" alt="' + attrs.alt + '" />';
            } else {
                angular.element(element[0]).replaceWith($compile('<md-icon ng-show="' + attrs.ngshow + '" md-svg-src="help" class="md-avatar ' + attrs.class + '"></md-icon>')(scope));
            }
        }
    };
});

/**
 * File read directive
 */
appDirective.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                };
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);

/**
 * Multiple fileread directive
 */
appDirective.directive("filereadmultiple", [function () {
    return {
        scope: {
            filereadmultiple: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                for(var key in changeEvent.target.files) {
                    if (typeof(changeEvent.target.files[key]) == "object") {
                        var reader = new FileReader();
                        reader.onload = function (loadEvent) {
                            scope.$apply(function () {
                                    scope.filereadmultiple.push(loadEvent.target.result);
                            });
                        };
                        reader.readAsDataURL(changeEvent.target.files[key]);
                    }
                }
            });
        }
    }
}]);
