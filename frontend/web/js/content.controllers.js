/**
 * Content application controllers
 */

'use strict';

var contentAppCtrl = angular.module('contentAppCtrl', ['ngMaterial', 'ngMessages', 'ngCookies']);

/**
 * The news controller - is charge for the news line in a index page
 */
contentAppCtrl.controller('newsCtrl', ['$scope', '$routeParams', 'news', 'lastNews',
    function($scope, $routeParams, news, lastNews){

        /**
         * Transaction the query to load news
         * @type {boolean}
         */
        $scope.doQuery = true;

        /**
         * Last news id
         */
        $scope.lastNews = $routeParams.lastNews;

        /**
         * Output news
         * @type {{newsObject}}
         */
        $scope.news = {};

        /**
         * Function the load more if event loadMore is occurs
         */
        $scope.loadMore = function() {
            if($scope.doQuery) {
                $scope.doQuery = false;
                //Is a first query?
                if ($scope.lastNews > 1) {
                    news.query({lastNews: $scope.lastNews}).$promise.then(
                    function(data) {
                        //Put data to news
                        for(var key in data) {
                            $scope.news.push(data[key]);
                        }
                        //If this is a bottom - finish select
                        if(data[data.length-1]) {
                            $scope.lastNews = data[data.length - 1].id;
                            $scope.doQuery = true;
                        }
                        else
                            $scope.doQuery = false;
                    });
                }
                else {
                    $scope.doQuery = false;
                    lastNews.query().$promise.then(
                    function(data) {
                        $scope.news = data;
                        $scope.lastNews = data[data.length-1].id;
                        $scope.doQuery = true;
                    });
                }
            }
        };

        /**
         * On load controller load last news
         */
        $scope.loadMore();

        /**
         * Event loadMore listener
         */
        $scope.$on('loadMore', function(event, fromParent) {
            $scope.loadMore();
        });

    }]);

/**
 * The promise controller.
 * If user select the interacting promise - handling this actions
 */
contentAppCtrl.controller('promiseCtrl', ['$scope', '$routeParams', 'promise',
        function($scope, $routeParams, promise){

            $scope.newsId = $routeParams.newsId;

            $scope.promise = [];

            promise.query({promiseId: $scope.newsId},
                function(data) {
                    $scope.promise = data;
                },
                function (response) {
                    $scope.catchHttpErrors(response);
                }
            );

}]);

/**
 * The Favorites controller.
 * Handling users favorites
 */
contentAppCtrl.controller('favoritesCtrl', ['$scope', '$routeParams', 'favorites',
    function($scope, $routeParams, favorites){

        $scope.lastNews = $routeParams.lastNews;

        $scope.setSelectedIndex = function (index) {
            $scope.lastNews = index;
        };

        $scope.getSelectedIndex = function () {
            return $scope.lastNews;
        };

        $scope.favorites = favorites.query();

}]);
