/**
 * Application init
 * @type {module}
 */

'use strict';

var application = angular.module('application', [
    'ngMaterial',
    'ngMessages',
    'ngRoute',
    'gettext',

    'appDirective',

    'contentServices',
    'authenticationServices',
    'profileServices',
    'userOwnerServices',

    'appCtrl',
    'contentAppCtrl'

]);

application.config(['$mdIconProvider',
    function($mdIconProvider) {
        $mdIconProvider
            .icon('account', '/vendor/bower/material-design-icons/action/svg/production/ic_account_box_48px.svg', 48)
            .icon('grade', '/vendor/bower/material-design-icons/action/svg/production/ic_grade_48px.svg', 48)
            .icon('bookmark', '/vendor/bower/material-design-icons/action/svg/production/ic_bookmark_48px.svg', 48)
            .icon('exit', '/vendor/bower/material-design-icons/action/svg/production/ic_exit_to_app_48px.svg', 48)
            .icon('close', '/vendor/bower/material-design-icons/content/svg/production/ic_clear_48px.svg', 48)
            .icon('entrance', '/vendor/bower/material-design-icons/action/svg/production/ic_input_48px.svg', 48)
            .icon('assignment', '/vendor/bower/material-design-icons/action/svg/production/ic_assignment_48px.svg', 48)
            .icon('what-shot', '/vendor/bower/material-design-icons/social/svg/production/ic_whatshot_48px.svg', 48)
            .icon('menu', '/vendor/bower/material-design-icons/navigation/svg/production/ic_menu_48px.svg', 48)
            .icon('help', '/vendor/bower/material-design-icons/action/svg/production/ic_help_48px.svg', 48)
            .icon('thumb-up', '/vendor/bower/material-design-icons/action/svg/production/ic_thumb_up_48px.svg', 48)
            .icon('thumb-down', '/vendor/bower/material-design-icons/action/svg/production/ic_thumb_down_48px.svg', 48)
            .icon('file-attach', '/vendor/bower/material-design-icons/editor/svg/production/ic_attach_file_48px.svg', 48)
            .icon('eye', '/vendor/bower/material-design-icons/action/svg/production/ic_visibility_48px.svg', 48)
            .icon('add', '/vendor/bower/material-design-icons/content/svg/production/ic_add_48px.svg', 48)
            .icon('notification', '/vendor/bower/material-design-icons/social/svg/production/ic_notifications_48px.svg', 48)
    }]);

application.config(['$routeProvider', '$locationProvider', '$controllerProvider',
    function($routeProvider, $locationProvider, $controllerProvider) {

        // remember mentioned function for later use
        application.registerCtrl = $controllerProvider.register;

        $routeProvider.
            //Main properties project
            when('/news/:startNews', {
                templateUrl: '/views/promises/news.html',
                controller: 'newsCtrl'
            }).
            when('/news', {
                templateUrl: '/views/promises/news.html',
                controller: 'newsCtrl'
            }).
            when('/home', {
                templateUrl: '/views/promises/news.html',
                controller: 'newsCtrl'
            }).
            when('/promise/:newsId', {
                templateUrl: '/views/promises/promise.html',
                controller: 'promiseCtrl'
            }).
            when('/favorites', {
                templateUrl: '/views/promises/favorites.html',
                controller: 'favoritesCtrl'
            }).
            when('/entrance', {
                templateUrl: '/views/main/entrance.html',
                controller: 'entranceCtrl'
            }).
            when('/registration', {
                templateUrl: '/views/main/registration.html',
                controller: 'registrationCtrl'
            }).
            when('/profile', {
                templateUrl: '/views/main/profile.html',
                controller: 'profileCtrl'
            }).
            when('/notvalidemail', {
                templateUrl: '/views/main/notvalidemail.html'
            }).
            //Personal data
            when('/my-promises', {
                templateUrl: '/views/user-owner/my-promises.html',
                controller: 'myPromisesCtrl'
            }).
            when('/my-promise/:id', {
                templateUrl: '/views/user-owner/my-promise.html',
                controller: 'myPromiseCtrl'
            }).
            when('/add-promise', {
                templateUrl: '/views/user-owner/add-promise.html',
                controller: 'addPromisesCtrl'
            }).
            when('/add-work-promise/:promise_id', {
                templateUrl: '/views/user-owner/add-work-promise.html',
                controller: 'addPromiseWorkCtrl'
            }).
            //Errors code
            when('/404', {
                templateUrl: '/views/errors/404.html'
            }).
            when('/403', {
                templateUrl: '/views/errors/403.html'
            }).
            when('/403-forbidden', {
                templateUrl: '/views/errors/403_forbidden.html'
            }).
            otherwise({
                templateUrl: '/views/promises/news.html',
                controller: 'newsCtrl'
            });

        // use the HTML5 History API
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }]);

/**
 * Applications constant
 */
application.constant('enums',
    {
        typeRate: {
            rating: 1,
            money: 2,
            reckoning: 3
        },
        videoHosting: [
            {
                name: "youtube",
                pregTest: /youtu\.?be/,
                pregMatch: /([\w-]+)$/,
                link: "https://www.youtube.com/embed/:link"
            },
            {
                name: "rutube",
                pregTest: /rutube\.ru/,
                pregMatch: /(\w+)\/?$/,
                link: "//rutube.ru/play/embed/:link"
            }
        ],
        videoStatus: {
            missing: 0,
            valid: 1,
            notValid: -1
        }
    }
);