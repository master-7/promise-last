'use strict';

/* Services */

var contentServices = angular.module('contentServices', ['ngResource']);

contentServices.factory('news', ['$resource',
	function($resource){
		return $resource('/api/return/news/:lastNews', {startNews:'@lastNews'}, {
			query: {method:'GET', isArray:true}
		});
	}]);

/**
 * Return promise by id
 */
contentServices.factory('promise', ['$resource',
	function($resource){
		return $resource('/api/return/shownews/:promiseId', {promiseId:'@promiseId'}, {
			query: {method:'GET'}
		});
	}]);

contentServices.factory('lastNews', ['$resource',
	function($resource){
		return $resource('/api/return/news/', {}, {
			query: {method:'GET', isArray:true}
		});
	}]);

contentServices.factory('favorites', ['$resource',
	function($resource){
		return $resource('/api/return/favorites/', {}, {
			query: {method:'GET', isArray:true}
		});
	}]);