'use strict';

/**
 * Error report services
 */
var errorReportServices = angular.module('errorReportServices', ['ngResource']);

/**
 * Get user promises
 */
errorReportServices.factory('error', ['$resource',
    function($resource){
        return $resource('/api/error', {}, {
            query: {method:'GET', isArray:true}
        });
    }]);