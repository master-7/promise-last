<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use \yii\web\Cookie;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\authclient\AuthAction;
use yii\authclient\ClientInterface;
use common\models\User;
use common\models\LoginForm;
use common\helpers\SaveImageHelper;
use master7\password\PassWordGenerator;
use yii\web\ServerErrorHttpException;

/**
 * AuthorizationController
 * Work with the users data and authentication user actions
 */
class AuthorizationController extends Controller
{
    /**
     * Auth action
     * @return array
     */
    public function actions()
    {
        return [
            'auth' => [
                'class' => AuthAction::className(),
                'successCallback' => [$this, 'onAuthSuccess']
            ],
        ];
    }

    /**
     * Method callback after auth
     * @param ClientInterface $client
     * @return string
     */
    public function onAuthSuccess(ClientInterface $client)
    {
        $attributesInAuth = $client->getUserAttributes();

        $attributes = [];

        switch($_GET['authclient']) {
            case 'vkontakte':
                $attributes = $this->handleDataVkontakte($attributesInAuth);
                break;
            case 'facebook':
                $attributes = $this->handleDataFacebook($attributesInAuth);
                break;
            case 'google':
                $attributes = $this->handleDataGoogle($attributesInAuth);
                break;
        }

        if(isset($attributes['username']))
        {
            $user = User::findByUsername($attributes['username']);
            $password = null;
            if($user)
            {
                $user->access_token = $user->accessTokenGenerator();
                $user->password = null;
            }
            else
            {
                if(isset($attributes['avatar']))
                {
                    $saveFile = new SaveImageHelper($attributes['avatar'], $attributes['avatar_link']);
                    $avatar = $saveFile->saveAvatar();
                }

                $password = PassWordGenerator::generate();

                $user = new User();
                $user->username = $attributes['username'];

                if(isset($avatar))
                    $user->avatar = $avatar;

                $user->name = $attributes['name'];
                $user->surname = $attributes['surname'];
                $user->password = $password;
                $user->sex = $attributes['sex'];
                $user->access_token = $user->accessTokenGenerator();
            }

            if($user->validate())
            {
                $user->save();
            }
            else
            {
                return json_encode($user->getErrors());
            }

            //After add new access token logout
            Yii::$app->user->logout();

            $cookies = Yii::$app->response->cookies;
            $cookies->remove('authKey');

            $cookies->add(new Cookie([
                'name' => 'authKey',
                'value' => $user->access_token,
                'httpOnly' => false
            ]));

            return $this->redirect('/');
        }
        else
        {
            return $this->redirect('/notvalidusername');
        }
    }

    /**
     * Function handle data vk
     * @param $data
     * @return array
     */
    private function handleDataVkontakte($data)
    {
        $attributes = [];

        switch($data['sex'])
        {
            case 1:
                $attributes['sex'] = User::SEX_FEMALE;
                break;
            case 2:
                $attributes['sex'] = User::SEX_MALE;
                break;
            default:
                $attributes['sex'] = User::SEX_UNKNOWN;
                break;
        }

        if(isset($data['email']))
            $attributes['username'] = $data['email'];

        $attributes['name'] = $data['first_name'];
        $attributes['surname'] = $data['last_name'];

        //Check is not empty photo
        $emptyPhoto = [
            "http://vk.com/images/camera_50.png",
            "http://vk.com/images/camera_100.png",
            "http://vk.com/images/camera_200.png",
            "http://vk.com/images/camera_a.gif",
            "http://vk.com/images/camera_b.gif",
            "http://vk.com/images/camera_c.gif"
        ];

        if(!in_array($data['photo'], $emptyPhoto)) {
            $userObj = json_decode(
                file_get_contents(
                    "http://api.vkontakte.ru/method/users.get?uids=" .
                    $data['id'] . "&fields=photo_max,status"
                )
            );

            if (!in_array($userObj->response[0]->photo_max, $emptyPhoto)) {
                $attributes['avatar_link'] = $userObj->response[0]->photo_max;
                $attributes['avatar'] = file_get_contents($attributes['avatar_link']);
            }
        }
        return $attributes;
    }

    /**
     * Function handle data facebook
     * @param $data
     * @return array
     */
    private function handleDataFacebook($data)
    {
        $attributes = [];
        switch($data['gender'])
        {
            case 'female':
                $attributes['sex'] = User::SEX_FEMALE;
                break;
            case "male":
                $attributes['sex'] = User::SEX_MALE;
                break;
            default:
                $attributes['sex'] = User::SEX_UNKNOWN;
                break;
        }

        if(isset($data['email']))
            $attributes['username'] = $data['email'];

        $attributes['name'] = $data['first_name'];
        $attributes['surname'] = $data['last_name'];
        $attributes['avatar_link'] = "http://graph.facebook.com/" . $data['id'] . "/picture?type=large";

        //Compare the getting avatar and empty photo
        $emptyPhoto = file_get_contents(Yii::getAlias('@webroot/images') . DIRECTORY_SEPARATOR . "facebookEmptyPhoto.jpg");
        $attributes['avatar'] = file_get_contents($attributes['avatar_link']);

        if($emptyPhoto == $attributes['avatar'])
        {
            unset($attributes['avatar']);
            unset($attributes['avatar_link']);
        }
        return $attributes;
    }

    /**
     * Function handle data google
     * @param $data
     * @return array
     */
    private function handleDataGoogle($data)
    {
        $attributes = [];
        switch($data['gender'])
        {
            case 'female':
                $attributes['sex'] = User::SEX_FEMALE;
                break;
            case "male":
                $attributes['sex'] = User::SEX_MALE;
                break;
            default:
                $attributes['sex'] = User::SEX_UNKNOWN;
                break;
        }

        if(isset($data['emails']))
        {
            $attributes['email'] = null;

            foreach($data['emails'] as $key => $val)
            {
                if($val['type'] == 'account')
                {
                    $attributes['username'] = $val['value'];
                }
            }
        }

        $attributes['name'] = $data['name']['givenName'];
        $attributes['surname'] = $data['name']['familyName'];
        //Check photo on empty
        if($data['image']['url'] !=
            "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50")
        {
            $data['image']['url'] = preg_replace('\?sz=\d+$', '', $data['image']['url']);
            $attributes['avatar_link'] = $data['image']['url'] . '?sz=200';
            $attributes['avatar'] = file_get_contents($attributes['avatar_link']);
        }
        return $attributes;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['auth', 'authenticate', 'registration', 'logout'],
                        'allow' => true
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Registration action by form
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws ServerErrorHttpException
     * @throws \yii\base\ErrorException
     */
    public function actionRegistration()
    {
        $data = json_decode(file_get_contents("php://input"));

        if (isset($data->username))
        {
            if (isset($data->avatar))
            {
                preg_match('/data:image\/(?P<type>\w+);base64,/', $data->avatar, $type);
                if (isset($type['type']))
                {
                    $avatar = preg_replace('/data:image\/\w+;base64,/', '', $data->avatar);
                    $avatar = new SaveImageHelper(base64_decode($avatar));
                    $data->avatar = $avatar->saveAvatar('.' . $type['type']);
                }
                else
                {
                    unset($data->avatar);
                }
            }

            $user = new User();
            $user->username = $data->username;

            if (isset($data->avatar))
                $user->avatar = $data->avatar;

            $user->name = $data->name;
            $user->surname = $data->surname;
            $user->password = $data->password;
            $user->sex = $data->sex;
            $user->access_token = $user->accessTokenGenerator();

            if ($user->validate() && $user->save())
            {
                Yii::$app->user->logout();

                $cookies = Yii::$app->response->cookies;
                $cookies->remove('authKey');

                $cookies->add(new Cookie([
                    'name' => 'authKey',
                    'value' => $user->access_token,
                    'httpOnly' => false
                ]));

                return true;
            }
            else
            {
                if (isset($data->avatar))
                    SaveImageHelper::deleteAvatar($data->avatar);

                return json_encode($user->getErrors());
            }
        }
        else
        {
            throw new ServerErrorHttpException('Email is a required field');
        }
    }

    /**
     * Authenticate user by user params or access token
     * @param $username
     * @param $password
     * @param $rememberMe
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function actionAuthenticate($username = null, $password = null, $rememberMe = false)
    {
        if (!Yii::$app->user->isGuest)
        {
            return json_encode(Yii::$app->user->identity->getClientInfo());
        }

        $form = [];
        $user = null;

        if(isset(Yii::$app->request->cookies['authKey']))
        {
            $user = User::findIdentityByAccessToken(Yii::$app->request->cookies['authKey']->value);

            $tokenExists = !empty($user->access_token);

            $edit = new \DateTime($user->edit_date);
            $interval = $edit->diff(new \DateTime());
            $tokenHasNotExpired = (int)$interval->format('%R%a') <= LoginForm::SAVE_DAYS;

            if($tokenExists && $tokenHasNotExpired)
            {
                Yii::$app->user->login($user, 3600 * 24 * LoginForm::SAVE_DAYS);
            }
            else
            {
                throw new ForbiddenHttpException('Auth token is not valid!');
            }
        }
        else
        {
            $form['LoginForm'] = [
                'username' => $username,
                'password' => $password,
                'rememberMe' => $rememberMe ? 1 : 0
            ];

            $model = new LoginForm();

            if($model->load($form) && $model->login())
            {
                if(!$user)
                    $user = User::findByUsername($username);

                $user->access_token = $user->accessTokenGenerator();
                $user->password = null;
                $user->save();

                $cookies = Yii::$app->response->cookies;
                $cookies->remove('authKey');

                $cookies->add(new Cookie([
                    'name' => 'authKey',
                    'value' => $user->access_token,
                    'httpOnly' => false
                ]));
            }
            else
                throw new ForbiddenHttpException(json_encode($model->getErrors()));
        }

        return json_encode(Yii::$app->user->identity->getClientInfo());
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
