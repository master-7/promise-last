<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\ErrorReports;

/**
 * ErrorController
 * Allow the work users with application
 */
class ErrorController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['*']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['post']
                ],
            ],
        ];
    }

    /**
     * Generate the error reports
     * @return bool|string
     */
    public function actionIndex()
    {
        $data['ErrorReports'] = json_decode(file_get_contents("php://input"), true);

        $model = new ErrorReports();
        $model->load($data);
        if($model->validate() && $model->save())
            return true;
        else
            return json_encode($model->getErrors());
    }
}
