<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\User;
use common\helpers\SaveImageHelper;
use yii\web\ForbiddenHttpException;

/**
 * RestController
 * Allow the work users with application
 */
class RestController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['updateprofile'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'updateprofile' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Action update the user profile
     * @throws ForbiddenHttpException
     * @throws \yii\base\ErrorException
     */
    public function actionUpdateprofile()
    {
        $data = json_decode(file_get_contents("php://input"));

        if(isset($data->checkPass)) {
            $model = Yii::$app->user->identity;

            if(!$model->validatePassword($data->checkPass))
                throw new ForbiddenHttpException("You enter wrong password!");

            if($newAvatar = $data->avatar) {
                preg_match('/data:image\/(?P<type>\w+);base64,/', $newAvatar, $type);
                if(isset($type['type'])) {
                    $newAvatar = preg_replace('/data:image\/\w+;base64,/', '', $newAvatar);
                    $saveAvatar = new SaveImageHelper(base64_decode($newAvatar));
                    $data->avatar = $saveAvatar->saveAvatar('.' . $type['type']);
                }
                else {
                    unset($data->avatar);
                }
            }

            if(isset($data->password))
                $model->password = $data->password;
            else
                $model->password = null;

            if(isset($data->name))
                $model->name = $data->name;

            if(isset($data->surname))
                $model->surname = $data->surname;

            if(isset($data->email))
                $model->username = $data->username;

            if(isset($data->sex))
                $model->sex = $data->sex;

            if(isset($data->avatar)) {
                $oldAvatar = $model->avatar;
                $model->avatar = $data->avatar;
            }

            if($model->validate() && $model->save()) {
                if(isset($oldAvatar))
                    SaveImageHelper::deleteAvatar($oldAvatar);
                return true;
            }
            else {
                SaveImageHelper::deleteAvatar($model->avatar);
                return json_encode($model->getErrors());
            }
        }
        throw new ForbiddenHttpException("Enter password!");
    }
}
