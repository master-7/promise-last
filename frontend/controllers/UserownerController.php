<?php

namespace frontend\controllers;

use common\models\Complaint;
use common\models\MultimediaGallery;
use common\models\MultimediaResource;
use common\models\PromiseCache;
use common\models\PromiseRate;
use common\models\PromiseWork;
use common\models\PromiseWorkCache;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use common\models\Promise;
use common\helpers\SaveImageHelper;
use yii\web\ForbiddenHttpException;

class UserownerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['promises', 'promise', 'complaint', 'workpromise'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'complaint' => ['post'],
                    'promises' => ['post', 'get'],
                    'workpromise' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     *  Save the user complaint
     */
    public function actionComplaint()
    {
        $data['Complaint'] = json_decode(file_get_contents("php://input"), true);
        $model = new Complaint();
        $model->load($data);
        if($model->validate() && $model->save()) {
            return true;
        } else {
            return json_encode($model->getErrors());
        }
    }

    /**
     * Get promise by id for user owner
     * @param $id
     * @return string
     */
    public function actionPromise($id)
    {
        $promise = PromiseCache::find()
            ->withUser(Yii::$app->user->id)
            ->withPromise($id)
            ->one();

        if(isset($promise->json_data))
            return $promise->json_data;
    }

    /**
     * Work promise action
     * @param null $id promise work id
     * @return string|void
     * @throws BadRequestHttpException
     */
    public function actionWorkpromise($id = null)
    {
        switch($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                if($id)
                    return $this->getWorkPromise($id);
                throw new BadRequestHttpException("Missing required parameters: id");
                break;
            case 'POST':
                $data = json_decode(file_get_contents("php://input"), true);
                return $this->saveWorkPromise($data);
                break;
        }
    }

    /**
     * Get the promise work
     * @param $promise_work_id
     * @return string
     */
    public function getWorkPromise($promise_work_id)
    {
        PromiseWorkCache::doAllPromiseWorkCache();
        $promiseWork = PromiseWorkCache::findOne(["work_id" => $promise_work_id]);
        if(isset($promiseWork->json_data))
            return $promiseWork->json_data;
    }

    /**
     * Save the user promise
     * @param $data
     * @return array|bool
     * @throws BadRequestHttpException
     * @throws ForbiddenHttpException
     */
    public function saveWorkPromise($data)
    {
        if(isset($data["promise_id"])) {
            $isUserPromise = Promise::find()
                ->withPk($data["promise_id"])
                ->withUserOwner(Yii::$app->user->id)
                ->exists();
            if($isUserPromise){
                $galleryId = 0;
                if ($data["gallery"]["photos"] || $data["gallery"]["videos"]) {
                    $multimedia = new MultimediaGallery();
                    $multimedia->save();
                    $galleryId = $multimedia->id;

                    //Save the photos
                    if ($data["gallery"]["photos"]) {
                        foreach ($data["gallery"]["photos"] as $key => $val) {
                            $imagePath = null;
                            $type = SaveImageHelper::getTypeByBase64($val);
                            $val = SaveImageHelper::clearBase64string($val);
                            if ($type) {
                                $saveAvatar = new SaveImageHelper(base64_decode($val));
                                $imagePath = $saveAvatar->savePromiseWorkImage('.' . $type);
                            }
                            if ($imagePath) {
                                $multimediaResource = new MultimediaResource();
                                $multimediaResource->gallery_id = $galleryId;
                                $multimediaResource->type = MultimediaResource::TYPE_PHOTO;
                                $multimediaResource->link = $imagePath;
                                $multimediaResource->save();
                            }
                        }
                    }

                    //Save the videos
                    if ($data["gallery"]["videos"]) {
                        foreach ($data["gallery"]["videos"] as $key => $val) {
                            $multimediaResource = new MultimediaResource();
                            $multimediaResource->gallery_id = $galleryId;
                            $multimediaResource->type = MultimediaResource::TYPE_VIDEO;
                            $multimediaResource->link = $val;
                            $multimediaResource->save();
                        }
                    }
                }

                $logo = null;
                if(isset($data["logo"])) {
                    $type = SaveImageHelper::getTypeByBase64($data["logo"]);
                    $data["logo"] = SaveImageHelper::clearBase64string($data["logo"]);
                    if ($type) {
                        $saveAvatar = new SaveImageHelper(base64_decode($data["logo"]));
                        $logo = $saveAvatar->savePromiseWorkImage('.' . $type);
                    }
                }

                $promiseWorkData['PromiseWork'] = [
                    "promise_id" => $data["promise_id"],
                    "user_id" => Yii::$app->user->id,
                    "logo" => $logo,
                    "title" => $data["title"],
                    "description" => $data["description"],
                    "multimedia" => $galleryId,
                ];
                $promiseWork = new PromiseWork();
                if($promiseWork->load($promiseWorkData) & $promiseWork->save())
                    return true;
                else
                    return json_encode($promiseWork->getErrors());
            }
            throw new ForbiddenHttpException("Is not you promise!");
        }
        throw new BadRequestHttpException("Field promise_id is required!");
    }

    /**
     * @return array|Promise|null
     */
    public function actionPromises()
    {
        switch($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                return $this->getPromises();
                break;
            case 'POST':
                $data = json_decode(file_get_contents("php://input"), true);
                return $this->savePromise($data);
                break;
        }
    }

    /**
     * Function return the promise for current user
     * @return array|Promise|null
     */
    public function getPromises()
    {
        $news = PromiseCache::find()
            ->select('json_data')
            ->withUser(Yii::$app->user->id)
            ->orderBy('promise_id DESC')
            ->asArray()
            ->all();

        foreach($news as $key => $value) {
            $news[$key] = array_values($value)[0];
        }

        return json_encode($news);
    }

    /**
     * Save the promise
     * @param $data
     * @return bool|string
     * @throws \yii\base\ErrorException
     */
    public function savePromise($data)
    {
        if(isset($data['image'])) {
            preg_match('/data:image\/(?P<type>\w+);base64,/', $data['image'], $type);
            if(isset($type)) {
                $data['image'] = preg_replace('/data:image\/\w+;base64,/', '', $data['image']);
                $saveAvatar = new SaveImageHelper(base64_decode($data['image']));
                $data['image_path'] = $saveAvatar->savePromiseImage('.' . $type);
            }
            unset($data['image']);
        }

        $data['user_id'] = Yii::$app->user->id;
        $data['user_surname'] = Yii::$app->user->identity->surname;
        $data['user_name'] = Yii::$app->user->identity->name;

        $promiseData['Promise'] = $data;

        $promise = new Promise();
        $promise->load($promiseData);

        if($promise->validate() && $promise->save()) {
            $rate = new PromiseRate();
            $rate->promise_id = $promise->id;
            $rate->type_rate = $data['typeRate'];
            $rate->rate = (string)$data['rate'];

            if($rate->validate() && $rate->save())
                return true;
            else {
                if($promise->image_path)
                    SaveImageHelper::deleteImage($promise->image_path);
                PromiseCache::deleteAll(['promise_id' => $promise->id]);
                Promise::deleteAll(['id' => $promise->id]);
                return json_encode($rate->getErrors());
            }
        }
        else {
            if($promise->image_path)
                SaveImageHelper::deleteImage($promise->image_path);
            return json_encode($promise->getErrors());
        }
    }

}