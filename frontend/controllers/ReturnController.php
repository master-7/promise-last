<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use Yii\helpers\ArrayHelper;
use common\models\PromiseCache;
use common\models\PromiseWork;
use common\models\Promise;
use common\models\CommentPromise;
use common\models\AppMenu;

/**
 * ReturnController
 * Return all information for the application
 */
class ReturnController extends Controller
{
    const LIMIT_NEWS = 10;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['news', 'shownews', 'promisework'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'menu' => ['get'],
                    'news' => ['get'],
                    'shownews' => ['get'],
                    'promisework' => ['get'],
                ],
            ],
        ];
    }

    /**
     * Return last news
     * If id is empty return last news
     * If id not empty return 10 news - defined constant self::LIMIT_NEWS less then variable $id
     * @param null $id
     * @return null|string
     * @throws Yii\web\NotFoundHttpException
     */
    public function actionNews($id = null)
    {
        /**
         * @todo Drop this method before upload in production
         */
//        PromiseCache::doAllPromiseCache();

        if($id > 1 || $id == null) {
            $news = Promise::find();

            if ($id)
                $news->lessThenValueAndLimit($id, self::LIMIT_NEWS);
            else
                $news->limit(self::LIMIT_NEWS);

            $result = $news->orderBy('prm_promise.id DESC')
                ->asArray()
                ->all();

            return json_encode( $result );
        } else {
            throw new NotFoundHttpException;
        }
    }

    /**
     * Show news by id
     * @param $id
     * @return string
     * @throws Yii\web\NotFoundHttpException
     */
    public function actionShownews($id)
    {
        /**
         * @todo Drop this method before upload in production
         */
//        PromiseCache::doAllPromiseCache();

        $comments = PromiseCache::findOne($id);
        if(isset($comments->json_data)) {
            return $comments->json_data;
        }
        throw new NotFoundHttpException;
    }

    /**
     * Show promise work by id
     * @param $id
     * @return string
     */
    public function actionPromisework($id)
    {
        return json_encode(
            PromiseWork::find()
                ->joinWith('comment')
                ->joinWith('comment.promiseWorkPostComments')
                ->withPk($id)
                ->asArray()
                ->one()
        );
    }

    /**
     * Show all comments by promise_id
     * @param $id
     * @return string
     */
    public function actionComment($id)
    {
        return json_encode(
            CommentPromise::find()
                ->withNews($id)
                ->asArray()
                ->all()
        );
    }
}
