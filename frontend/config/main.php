<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'homeUrl' => '/home',
    'components' => [
        'request' => [
            'baseUrl' => '/api',
            'enableCsrfValidation'=>false
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vkontakte' => [
                    'class'        => 'yii\authclient\clients\VKontakte',
                    'clientId'     => '5013167',
                    'clientSecret' => 'H6ZSvxwCR40FAxAY1KRB',
                    'scope' => 'email'
                ],
                'facebook' => [
                    'class'        => 'yii\authclient\clients\Facebook',
                    'clientId'     => '982437251778266',
                    'clientSecret' => 'f34f52fbd6c1f38eaa43ee01d8ca35c6',
                    'attributeNames' => ['id', 'cover', 'first_name', 'last_name', 'gender', 'email']
                ],
                'google' => [
                    'class'        => 'yii\authclient\clients\GoogleOAuth',
                    'clientId'     => '98679397619-1a9sgjdgb4tkn38q4u6r8dr7j05k9rgf.apps.googleusercontent.com',
                    'clientSecret' => 'YzKITArRuFx3XQxDdJ6sj5e5',
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>'=>'<controller>/<view>',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
