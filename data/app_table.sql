
-- -----------------------------------------------------
-- Table `prm_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_user` ;

CREATE TABLE IF NOT EXISTS `prm_user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(128) NOT NULL,
  `surname` VARCHAR(50) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `logo` VARCHAR(255) DEFAULT NULL,
  `sex` INT(1) NOT NULL,
  `online` INT(1) NOT NULL DEFAULT 0,
  `city_id` INT(10) NOT NULL DEFAULT 0,
  `password` TEXT NOT NULL,
  `salt` TEXT NOT NULL,
  `password_reset_token` VARCHAR(50) DEFAULT NULL,
  `auth_key` VARCHAR(255) DEFAULT NULL,
  `access_token` VARCHAR(255) DEFAULT NULL,
  `status` INT(1) NOT NULL DEFAULT 1,
  `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `username`, `surname`, `name`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `fk_prm_user_to_prm_city` (`city_id` ASC),
  CONSTRAINT `fk_prm_user_to_prm_city`
  FOREIGN KEY (`city_id`)
  REFERENCES `prm_city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `prm_user_send_invite_by_username`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_user_send_invite_by_username` ;

CREATE TABLE IF NOT EXISTS `prm_user_send_invite_by_username` (
  `id_user` INT NOT NULL,
  `refusing_to_send` INT(1) NOT NULL DEFAULT 0,
  `login` VARCHAR(255) NOT NULL,
  `pass` VARCHAR(255) NOT NULL,
  `sending_success` INT(1) NOT NULL DEFAULT 0,
  `edit_date` DATETIME NOT NULL,
  PRIMARY KEY (`id_user`),
  CONSTRAINT `fk_prm_user_send_invite_to_username`
  FOREIGN KEY (`id_user`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_friends`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_friends` ;

CREATE TABLE IF NOT EXISTS `prm_friends` (
  `user` INT NOT NULL,
  `friend` INT NOT NULL,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`user`, `friend`),
  UNIQUE INDEX `user_UNIQUE` (`user` ASC),
  UNIQUE INDEX `friend_UNIQUE` (`friend` ASC),
  CONSTRAINT `fk_prm_friends_1`
  FOREIGN KEY (`user`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_friends_2`
  FOREIGN KEY (`friend`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_friends_cache`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_friends_cache` ;

CREATE TABLE IF NOT EXISTS `prm_friends_cache` (
  `user_id` INT NOT NULL,
  `friends_json` LONGTEXT NOT NULL,
  `edit_date` DATETIME NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_prm_friends_cache_1`
  FOREIGN KEY (`user_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_promise`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_promise` ;

CREATE TABLE IF NOT EXISTS `prm_promise` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `user_surname` VARCHAR(50) NOT NULL,
  `user_name` VARCHAR(50) NOT NULL,
  `image_path` VARCHAR(45) NOT NULL,
  `rate` INT NOT NULL,
  `title` VARCHAR(50) NOT NULL,
  `short_text` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `success` VARCHAR(45) NOT NULL,
  `views` INT NOT NULL DEFAULT 0,
  `create_date` DATETIME NOT NULL,
  `start_date` DATETIME NOT NULL,
  `deadline` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_prm_promise_1`
  FOREIGN KEY (`user_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB CHARSET=UTF8;

-- -----------------------------------------------------
-- Table `prm_promise_assessment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_promise_assessment` ;

CREATE TABLE IF NOT EXISTS `prm_promise_assessment` (
  `promise_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `assessment` INT(1) NOT NULL,
  `active` INT(1) NOT NULL DEFAULT 1,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`promise_id`, `user_id`),
  INDEX `fk_prm_common_promise_assessment_1_idx` (`user_id` ASC),
  CONSTRAINT `fk_prm_promise_assessment_1`
  FOREIGN KEY (`user_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_promise_assessment_2`
  FOREIGN KEY (`promise_id`)
  REFERENCES `prm_promise` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB CHARSET=UTF8;


-- -----------------------------------------------------
-- Table `prm_prm_views`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_prm_views` ;

CREATE TABLE IF NOT EXISTS `prm_prm_views` (
  `user_id` INT NOT NULL,
  `prm_id` INT NOT NULL,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`user_id`, `prm_id`),
  INDEX `fk_prm_views_2` (`prm_id` ASC),
  CONSTRAINT `fk_prm_views_1`
  FOREIGN KEY (`user_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_views_2`
  FOREIGN KEY (`prm_id`)
  REFERENCES `prm_promise` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_favorites`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_favorites` ;

CREATE TABLE IF NOT EXISTS `prm_favorites` (
  `user` INT NOT NULL,
  `promise` INT NOT NULL,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`user`, `promise`),
  INDEX `fk_prm_favorites_2_idx` (`promise` ASC),
  CONSTRAINT `fk_prm_favorites_1`
  FOREIGN KEY (`user`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_favorites_2`
  FOREIGN KEY (`promise`)
  REFERENCES `prm_promise` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_favorites_cache`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_favorites_cache` ;

CREATE TABLE IF NOT EXISTS `prm_favorites_cache` (
  `user_id` INT NOT NULL,
  `prm_json` LONGTEXT NOT NULL,
  `edit_date` DATETIME NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_prm_favorites_1`
  FOREIGN KEY (`user_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_comment_promise`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_comment_promise` ;

CREATE TABLE IF NOT EXISTS `prm_comment_promise` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `news_id` INT NOT NULL,
  `author_id` INT NOT NULL,
  `author_hidden` INT(1) NOT NULL,
  `comment` TEXT NOT NULL,
  `assessment` INT(1) NOT NULL,
  `positive` INT NOT NULL,
  `negative` INT NOT NULL,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_prm_comment_promise_1` (`news_id` ASC),
  INDEX `fk_prm_comment_promise_2` (`author_id` ASC),
  CONSTRAINT `fk_prm_comment_promise_1`
  FOREIGN KEY (`news_id`)
  REFERENCES `prm_promise` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_comment_promise_2`
  FOREIGN KEY (`author_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_comment_promise_assessment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_comment_promise_assessment` ;

CREATE TABLE IF NOT EXISTS `prm_comment_promise_assessment` (
  `comment_promise` INT NOT NULL,
  `user_id` INT NOT NULL,
  `assessment` INT(1) NOT NULL,
  `create_date` DATETIME NOT NULL,
  INDEX `fk_prm_comment_promise_assessment_2` (`user_id` ASC),
  INDEX `fk_prm_comment_promise_assessment_1_idx` (`comment_promise` ASC),
  CONSTRAINT `fk_prm_comment_promise_assessment_1`
  FOREIGN KEY (`comment_promise`)
  REFERENCES `prm_comment_promise` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_comment_promise_assessment_2`
  FOREIGN KEY (`user_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_comment_post`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_comment_post` ;

CREATE TABLE IF NOT EXISTS `prm_comment_post` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `comment_id` INT NOT NULL,
  `post_id` INT NOT NULL,
  `author_id` INT NOT NULL,
  `author_hidden` INT(1) NOT NULL,
  `comment` TEXT NOT NULL,
  `assessment` INT(1) NOT NULL,
  `positive` INT NOT NULL,
  `negative` INT NOT NULL,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_prm_comment_post_1` (`comment_id` ASC),
  INDEX `fk_prm_comment_post_2` (`post_id` ASC),
  INDEX `fk_prm_comment_post_3` (`author_id` ASC),
  CONSTRAINT `fk_prm_comment_post_1`
  FOREIGN KEY (`comment_id`)
  REFERENCES `prm_comment_promise` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_comment_post_2`
  FOREIGN KEY (`post_id`)
  REFERENCES `prm_comment_post` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_comment_post_3`
  FOREIGN KEY (`author_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_comment_post_assessment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_comment_post_assessment` ;

CREATE TABLE IF NOT EXISTS `prm_comment_post_assessment` (
  `comment_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `assessment` INT(1) NOT NULL,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`comment_id`, `user_id`),
  INDEX `fk_prm_comment_post_assessment_2` (`user_id` ASC),
  CONSTRAINT `fk_prm_comment_post_assessment_1`
  FOREIGN KEY (`comment_id`)
  REFERENCES `prm_comment_post` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_comment_post_assessment_2`
  FOREIGN KEY (`user_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_personal_msg`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_personal_msg` ;

CREATE TABLE IF NOT EXISTS `prm_personal_msg` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user1_id` INT NOT NULL,
  `user2_id` INT NOT NULL,
  `msg` TEXT NOT NULL,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT INDEX (`msg` ASC),
  INDEX `fk_prm_personal_msg_1` (`user1_id` ASC),
  INDEX `fk_prm_personal_msg_2` (`user2_id` ASC),
  CONSTRAINT `fk_prm_personal_msg_1`
  FOREIGN KEY (`user1_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_personal_msg_2`
  FOREIGN KEY (`user2_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_chat_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_chat_type` ;

CREATE TABLE IF NOT EXISTS `prm_chat_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_chat`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_chat` ;

CREATE TABLE IF NOT EXISTS `prm_chat` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type_id` INT(2) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_prm_chat_1_idx` (`type_id` ASC),
  CONSTRAINT `fk_prm_chat_1`
  FOREIGN KEY (`type_id`)
  REFERENCES `prm_chat_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_user_chat_connections`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_user_chat_connections` ;

CREATE TABLE IF NOT EXISTS `prm_user_chat_connections` (
  `chat_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`chat_id`, `user_id`),
  INDEX `fk_prm_user_chat_connections_2_idx` (`user_id` ASC),
  CONSTRAINT `fk_prm_user_chat_connections_1`
  FOREIGN KEY (`chat_id`)
  REFERENCES `prm_chat` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_user_chat_connections_2`
  FOREIGN KEY (`user_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_chat_messages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_chat_messages` ;

CREATE TABLE IF NOT EXISTS `prm_chat_messages` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `chat_id` INT NOT NULL,
  `owner_id` INT NOT NULL,
  `msg` TEXT NOT NULL,
  `is_reading` INT(1) NOT NULL DEFAULT 0,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_prm_chat_messages_1_idx` (`chat_id` ASC),
  INDEX `fk_prm_chat_messages_2_idx` (`owner_id` ASC),
  CONSTRAINT `fk_prm_chat_messages_1`
  FOREIGN KEY (`chat_id`)
  REFERENCES `prm_chat` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_chat_messages_2`
  FOREIGN KEY (`owner_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_chat_cache`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_chat_cache` ;

CREATE TABLE IF NOT EXISTS `prm_chat_cache` (
  `id_user` INT NOT NULL,
  `id_chat` INT NOT NULL,
  `json_data` LONGTEXT NOT NULL,
  `create_date` DATE NOT NULL,
  PRIMARY KEY (`id_user`, `id_chat`),
  INDEX `fk_prm_chat_cache_2_idx` (`id_chat` ASC),
  INDEX `fk_prm_chat_cache_3_idx` (`create_date` ASC),
  CONSTRAINT `fk_prm_chat_cache_1`
  FOREIGN KEY (`id_user`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_chat_cache_2`
  FOREIGN KEY (`id_chat`)
  REFERENCES `prm_chat` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_chat_user_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_chat_user_status` ;

CREATE TABLE IF NOT EXISTS `prm_chat_user_status` (
  `chat_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `status` INT(1) NOT NULL DEFAULT 0 COMMENT '0 - not active 1 - user write msg',
  PRIMARY KEY (`chat_id`, `user_id`),
  INDEX `fk_prm_chat_user_status_2_idx` (`user_id` ASC),
  CONSTRAINT `fk_prm_chat_user_status_1`
  FOREIGN KEY (`chat_id`)
  REFERENCES `prm_chat` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_chat_user_status_2`
  FOREIGN KEY (`user_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `prm_comment_promise_join_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_comment_promise_join_users` ;

CREATE TABLE IF NOT EXISTS `prm_comment_promise_join_users` (
  `comment_promise` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`comment_promise`, `user_id`),
  INDEX `fk_prm_comment_promise_join_users_2_idx` (`user_id` ASC),
  CONSTRAINT `fk_prm_comment_promise_join_users_1`
  FOREIGN KEY (`comment_promise`)
  REFERENCES `prm_comment_promise` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_comment_promise_join_users_2`
  FOREIGN KEY (`user_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB CHARSET=UTF8;

-- -----------------------------------------------------
-- Table `prm_multimedia_resourse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_multimedia_resourse` ;

CREATE TABLE IF NOT EXISTS `prm_multimedia_resourse` (
  `gallery_id` INT NOT NULL,
  `link` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  `type` INT(1) NOT NULL,
  PRIMARY KEY (`gallery_id`),
  CONSTRAINT `fk_prm_multimedia_resourse_1`
  FOREIGN KEY (`gallery_id`)
  REFERENCES `prm_multimedia_gallery` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `prm_multimedia_gallery`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_multimedia_gallery` ;

CREATE TABLE IF NOT EXISTS `prm_multimedia_gallery` (
  `id` INT NOT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `prm_promise_work`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_promise_work` ;

CREATE TABLE IF NOT EXISTS `prm_promise_work` (
  `id` INT NOT NULL,
  `promise_id` INT NOT NULL,
  `logo` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` MEDIUMTEXT NOT NULL,
  `multimedia` INT NOT NULL DEFAULT 0,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_prm_promise_work_1_idx` (`promise_id` ASC),
  INDEX `fk_prm_promise_work_2_idx` (`multimedia` ASC),
  CONSTRAINT `fk_prm_promise_work_1`
  FOREIGN KEY (`promise_id`)
  REFERENCES `prm_promise` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_promise_work_2`
  FOREIGN KEY (`multimedia`)
  REFERENCES `prm_multimedia_gallery` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `prm_promise_work_comment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_promise_work_comment` ;

CREATE TABLE IF NOT EXISTS `prm_promise_work_comment` (
  `id` INT NOT NULL,
  `promise_work_id` INT NOT NULL,
  `author_id` INT NOT NULL,
  `author_hidden` INT(1) NOT NULL,
  `comment` TEXT NOT NULL,
  `positive` INT NOT NULL,
  `negative` INT NOT NULL,
  `status` INT(1) NOT NULL DEFAULT 1,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_prm_promise_work_comment_1_idx` (`promise_work_id` ASC),
  INDEX `fk_prm_promise_work_comment_2_idx` (`author_id` ASC),
  CONSTRAINT `fk_prm_promise_work_comment_1`
  FOREIGN KEY (`promise_work_id`)
  REFERENCES `prm_promise_work` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_promise_work_comment_2`
  FOREIGN KEY (`author_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prm_promise_work_post_comment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `prm_promise_work_post_comment` ;

CREATE TABLE IF NOT EXISTS `prm_promise_work_post_comment` (
  `id` INT NOT NULL,
  `post_id` INT NOT NULL,
  `author_id` INT NOT NULL,
  `author_hidden` INT(1) NOT NULL,
  `comment` TEXT NOT NULL,
  `assessment` INT(1) NOT NULL,
  `positive` INT NOT NULL,
  `negative` INT NOT NULL,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_prm_promise_work_post_comment_1_idx` (`post_id` ASC),
  INDEX `fk_prm_promise_work_post_comment_2_idx` (`author_id` ASC),
  CONSTRAINT `fk_prm_promise_work_post_comment_1`
  FOREIGN KEY (`post_id`)
  REFERENCES `prm_promise_work_comment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prm_promise_work_post_comment_2`
  FOREIGN KEY (`author_id`)
  REFERENCES `prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;




-- -----------------------------------------------------
-- Table `promise`.`prm_entrance_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `promise`.`prm_entrance_history` ;

CREATE TABLE IF NOT EXISTS `promise`.`prm_entrance_history` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `ip_address` VARCHAR(45) NOT NULL,
  `date_time` DATETIME NOT NULL,
  `status` INT(1) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_prm_entrance_history_1_idx` (`user_id` ASC),
  CONSTRAINT `fk_prm_entrance_history_1`
  FOREIGN KEY (`user_id`)
  REFERENCES `promise`.`prm_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB DEFAULT CHARSET UTF8;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
