<?php

namespace console\controllers;

use \yii\console\Controller;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use console\models\UserOnline;

class UseronlineController extends Controller
{
    /**
     * Start web socket
     */
    public function actionRun()
    {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new UserOnline()
                )
            ),
            1337,
            '127.0.0.1'
        );

        $server->run();
    }
}