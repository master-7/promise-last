<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_051831_complaint_type extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO prm_complaint_type (id, type)
              VALUES
            (1, 'Оскорбительные высказывания'),
            (2, 'Порнографическое содержание'),
            (3, 'Призывы к насилию'),
            (4, 'Другое')
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM prm_complaint_type
        ");
    }
}
