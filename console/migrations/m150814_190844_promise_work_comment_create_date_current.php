<?php

use yii\db\Schema;
use yii\db\Migration;

class m150814_190844_promise_work_comment_create_date_current extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE prm_promise_work_comment
              CHANGE create_date create_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_promise_work_comment
              CHANGE create_date create_date DATETIME NOT NULL
        ");
    }
}
