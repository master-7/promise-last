<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_062943_error_reports extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_error_reports` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `comment` TEXT NOT NULL,
              `value` MEDIUMTEXT NOT NULL,
              `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `ip_address` VARCHAR(45) NOT NULL,
              PRIMARY KEY (`id`))
            ENGINE = InnoDB DEFAULT CHARSET UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_error_reports`
        ");
    }
}
