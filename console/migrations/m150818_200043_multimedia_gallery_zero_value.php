<?php

use yii\db\Schema;
use yii\db\Migration;

class m150818_200043_multimedia_gallery_zero_value extends Migration
{
    public function safeUp()
    {
        $this->execute("
            SET sql_mode='NO_AUTO_VALUE_ON_ZERO';
        ");
        $this->execute("
            INSERT INTO prm_multimedia_gallery (id) VALUES (0)
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM prm_multimedia_gallery WHERE id = 0
        ");
    }
}
