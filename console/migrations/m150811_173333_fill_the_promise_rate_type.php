<?php

use yii\db\Schema;
use yii\db\Migration;

class m150811_173333_fill_the_promise_rate_type extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO prm_promise_rate_type (id, name)
              VALUES
            (1, 'Рейтинг'),
            (2, 'Деньги'),
            (3, 'Расплата')
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM prm_promise_rate_type
        ");
    }
}
