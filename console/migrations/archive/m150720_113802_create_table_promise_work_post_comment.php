<?php

use yii\db\Schema;
use yii\db\Migration;

class m150720_113802_create_table_promise_work_post_comment extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_promise_work_post_comment` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `post_id` INT NOT NULL,
              `author_id` INT NOT NULL,
              `author_hidden` INT(1) NOT NULL,
              `comment` TEXT NOT NULL,
              `assessment` INT(1) NOT NULL,
              `positive` INT NOT NULL,
              `negative` INT NOT NULL,
              `create_date` DATETIME NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `fk_prm_promise_work_post_comment_1_idx` (`post_id` ASC),
              INDEX `fk_prm_promise_work_post_comment_2_idx` (`author_id` ASC),
              CONSTRAINT `fk_prm_promise_work_post_comment_1`
              FOREIGN KEY (`post_id`)
              REFERENCES `prm_promise_work_comment` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_promise_work_post_comment_2`
              FOREIGN KEY (`author_id`)
              REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
              ENGINE = InnoDB CHARSET=UTF8
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_promise_work_post_comment`
        ");
    }
}
