<?php

use yii\db\Schema;
use yii\db\Migration;

class m150727_121526_fix_app_menu extends Migration
{
    public function safeUp()
    {
        $this->execute("
            DELETE FROM pr_menu WHERE id IN (4, 3)
        ");
    }

    public function safeDown()
    {
        $this->execute("
            INSERT INTO pr_menu (id, title, icon, link) VALUES (4, 'Вход', 'entrance', 'entrance');
            INSERT INTO pr_menu (id, title, icon, link) VALUES (3, 'Профиль', 'account', 'profile');
        ");
    }
}
