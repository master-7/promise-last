<?php

use yii\db\Schema;
use yii\db\Migration;

class m150716_101108_create_table_comment_promise_join_users extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_comment_promise_join_users` (
              `comment_promise` INT NOT NULL,
              `user_id` INT NOT NULL,
              PRIMARY KEY (`comment_promise`, `user_id`),
              INDEX `fk_prm_comment_promise_join_users_2_idx` (`user_id` ASC),
              CONSTRAINT `fk_prm_comment_promise_join_users_1`
              FOREIGN KEY (`comment_promise`)
              REFERENCES `prm_comment_promise` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_comment_promise_join_users_2`
              FOREIGN KEY (`user_id`)
              REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
              ENGINE = InnoDB CHARSET=UTF8
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_comment_promise_join_users`
        ");
    }
}
