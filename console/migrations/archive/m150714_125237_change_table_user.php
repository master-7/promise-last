<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_125237_change_table_user extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `prm_user` ADD COLUMN `assessment` int(4) NOT NULL DEFAULT 10 AFTER `status`
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE `prm_user` DROP COLUMN `assessment`
        ");
    }
}
