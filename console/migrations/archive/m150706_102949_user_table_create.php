<?php

use yii\db\Schema;
use yii\db\Migration;

class m150706_102949_user_table_create extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_user` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `email` VARCHAR(128) NOT NULL,
              `surname` VARCHAR(50) NOT NULL,
              `name` VARCHAR(50) NOT NULL,
              `patronymic` VARCHAR(50) DEFAULT NULL,
              `city_id` INT(10) NOT NULL DEFAULT 0,
              `password` TEXT NOT NULL,
              `salt` TEXT NOT NULL,
              `password_reset_token` VARCHAR(50) DEFAULT NULL,
              `auth_key` VARCHAR(255) DEFAULT NULL,
              `access_token` VARCHAR(255) DEFAULT NULL,
              `status` INT(1) NOT NULL DEFAULT 1,
              `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `edit_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`, `email`, `surname`, `name`),
              UNIQUE INDEX `email_UNIQUE` (`email` ASC),
              INDEX `fk_prm_user_to_prm_city` (`city_id` ASC),
              CONSTRAINT `fk_prm_user_to_prm_city`
              FOREIGN KEY (`city_id`)
              REFERENCES `prm_city` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
              ENGINE = InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_user` ;
        ");
    }
}
