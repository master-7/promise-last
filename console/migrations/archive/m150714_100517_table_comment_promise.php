<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_100517_table_comment_promise extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_comment_promise` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `news_id` INT NOT NULL,
              `author_id` INT NOT NULL,
              `post_id` INT NULL,
              `author_hidden` INT(1) NOT NULL,
              `comment` TEXT NOT NULL,
              `assessment` INT(1) NOT NULL,
              `positive` INT NOT NULL,
              `negative` INT NOT NULL,
              `status` INT(1) NOT NULL DEFAULT 1,
              `create_date` DATETIME NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `fk_prm_comment_promise_1` (`news_id` ASC),
              INDEX `fk_prm_comment_promise_2` (`author_id` ASC),
              INDEX `fk_prm_comment_promise_3_idx` (`post_id` ASC),
              CONSTRAINT `fk_prm_comment_promise_1`
                FOREIGN KEY (`news_id`)
                REFERENCES `prm_promise` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_comment_promise_2`
                FOREIGN KEY (`author_id`)
                REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_comment_promise_3`
                FOREIGN KEY (`post_id`)
                REFERENCES `prm_comment_promise` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB CHARSET=UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS prm_comment_promise
        ");
    }
}
