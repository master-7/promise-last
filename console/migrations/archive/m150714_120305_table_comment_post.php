<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_120305_table_comment_post extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_comment_post` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `comment_id` INT NOT NULL,
              `post_id` INT,
              `author_id` INT NOT NULL,
              `author_hidden` INT(1) NOT NULL,
              `comment` TEXT NOT NULL,
              `assessment` INT(1) NOT NULL,
              `positive` INT NOT NULL,
              `negative` INT NOT NULL,
              `create_date` DATETIME NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `fk_prm_comment_post_1` (`comment_id` ASC),
              INDEX `fk_prm_comment_post_2` (`post_id` ASC),
              INDEX `fk_prm_comment_post_3` (`author_id` ASC),
              CONSTRAINT `fk_prm_comment_post_1`
              FOREIGN KEY (`comment_id`)
              REFERENCES `prm_comment_promise` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_comment_post_2`
              FOREIGN KEY (`post_id`)
              REFERENCES `prm_comment_post` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_comment_post_3`
              FOREIGN KEY (`author_id`)
              REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
          ENGINE = InnoDB CHARSET=UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS prm_comment_post
        ");
    }
}
