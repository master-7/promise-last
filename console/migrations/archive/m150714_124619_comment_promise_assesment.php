<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_124619_comment_promise_assesment extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_comment_promise_assessment` (
              `comment_promise` INT NOT NULL,
              `user_id` INT NOT NULL,
              `assessment` INT(1) NOT NULL,
              `create_date` DATETIME NOT NULL,
              INDEX `fk_prm_comment_promise_assessment_2` (`user_id` ASC),
              INDEX `fk_prm_comment_promise_assessment_1_idx` (`comment_promise` ASC),
              CONSTRAINT `fk_prm_comment_promise_assessment_1`
              FOREIGN KEY (`comment_promise`)
              REFERENCES `prm_comment_promise` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_comment_promise_assessment_2`
              FOREIGN KEY (`user_id`)
              REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
              ENGINE = InnoDB CHARSET=UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS prm_comment_promise_assessment
        ");
    }
}
