<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_123709_import_menu extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO `pr_menu` (`id`, `title`, `icon`, `link`) VALUES
                (1, 'Новое', 'what-shot', 'news'),
                (2, 'Избранное', 'bookmark', 'favorites'),
                (3, 'Профиль', 'account', 'profile'),
                (4, 'Вход', 'entrance', 'entrance');
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM `pr_menu`
        ");
    }
}
