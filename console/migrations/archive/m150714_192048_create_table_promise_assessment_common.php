<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_192048_create_table_promise_assessment_common extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_promise_assessment_common` (
              `promise_id` INT NOT NULL,
              `user_id` INT NOT NULL,
              `assessment` INT(1) NOT NULL,
              `active` INT(1) NOT NULL DEFAULT 1,
              `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`promise_id`, `user_id`),
              INDEX `fk_prm_common_promise_assessment_1_idx` (`user_id` ASC),
              CONSTRAINT `fk_prm_promise_assessment_common_1`
              FOREIGN KEY (`user_id`)
              REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_promise_assessment_common_2`
              FOREIGN KEY (`promise_id`)
              REFERENCES `prm_promise` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
              ENGINE = InnoDB CHARSET=UTF8
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_promise_assessment_common`
        ");
    }
}
