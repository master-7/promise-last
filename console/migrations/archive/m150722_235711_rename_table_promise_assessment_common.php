<?php

use yii\db\Schema;
use yii\db\Migration;

class m150722_235711_rename_table_promise_assessment_common extends Migration
{
    public function safeUp()
    {
        $this->execute("
            RENAME TABLE prm_promise_assessment_common TO prm_promise_assessment
        ");
    }

    public function safeDown()
    {
        $this->execute("
            RENAME TABLE prm_promise_assessment TO prm_promise_assessment_common
        ");
    }
}
