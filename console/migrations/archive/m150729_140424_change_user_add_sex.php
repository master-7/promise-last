<?php

use yii\db\Schema;
use yii\db\Migration;

class m150729_140424_change_user_add_sex extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE prm_user ADD COLUMN sex INT(1) DEFAULT 0 AFTER avatar
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_user DROP COLUMN sex
        ");
    }
}
