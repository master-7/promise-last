<?php

use yii\db\Schema;
use yii\db\Migration;

class m150721_110300_zero_user extends Migration
{
    public function safeUp()
    {
        $this->execute("
            SET sql_mode='NO_AUTO_VALUE_ON_ZERO';
        ");
        $this->execute("
            INSERT INTO prm_user
              (id, email, surname, name, patronymic,
                city_id, password, salt, password_reset_token,
                auth_key, access_token, status, assessment, create_date, edit_date)
                  VALUES
            (0, 'Пользователь скрыл', 'Анонимный', 'Аноним', 'отсутствует',
                0, 'отсутствует', 'отсутствует', null, null, null, 1, 10,
                '2015-07-21 14:02:08', '2015-07-21 14:02:08');
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM prm_user WHERE id = 0
        ");
    }
}
