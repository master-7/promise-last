<?php

use yii\db\Schema;
use yii\db\Migration;

class m150722_234738_drop_wrong_table extends Migration
{
    public function safeUp()
    {
        $this->execute("
            DROP TABLE `prm_comment_post_assessment`
        ");
        $this->execute("
            DROP TABLE `prm_comment_post`
        ");
        $this->execute("
            DROP TABLE `prm_comment_promise_join_users`
        ");
        $this->execute("
            DROP TABLE `prm_promise_work_post_comment`
        ");
    }

    public function safeDown()
    {
        $this->execute("
            CREATE TABLE `prm_comment_post` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `comment_id` int(11) NOT NULL,
              `post_id` int(11) DEFAULT NULL,
              `author_id` int(11) NOT NULL,
              `author_hidden` int(1) NOT NULL,
              `comment` text NOT NULL,
              `assessment` int(1) NOT NULL,
              `positive` int(11) NOT NULL,
              `negative` int(11) NOT NULL,
              `create_date` datetime NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_prm_comment_post_1` (`comment_id`),
              KEY `fk_prm_comment_post_2` (`post_id`),
              KEY `fk_prm_comment_post_3` (`author_id`),
              CONSTRAINT `fk_prm_comment_post_1` FOREIGN KEY (`comment_id`) REFERENCES `prm_comment_promise` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_comment_post_2` FOREIGN KEY (`post_id`) REFERENCES `prm_comment_post` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_comment_post_3` FOREIGN KEY (`author_id`) REFERENCES `prm_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
        $this->execute("
            CREATE TABLE `prm_comment_post_assessment` (
              `comment_id` int(11) NOT NULL,
              `user_id` int(11) NOT NULL,
              `assessment` int(1) NOT NULL,
              `create_date` datetime NOT NULL,
              PRIMARY KEY (`comment_id`,`user_id`),
              KEY `fk_prm_comment_post_assessment_2` (`user_id`),
              CONSTRAINT `fk_prm_comment_post_assessment_1` FOREIGN KEY (`comment_id`) REFERENCES `prm_comment_post` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_comment_post_assessment_2` FOREIGN KEY (`user_id`) REFERENCES `prm_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
        $this->execute("
            CREATE TABLE `prm_comment_promise_join_users` (
              `comment_promise` int(11) NOT NULL,
              `user_id` int(11) NOT NULL,
              PRIMARY KEY (`comment_promise`,`user_id`),
              KEY `fk_prm_comment_promise_join_users_2_idx` (`user_id`),
              CONSTRAINT `fk_prm_comment_promise_join_users_1` FOREIGN KEY (`comment_promise`) REFERENCES `prm_comment_promise` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_comment_promise_join_users_2` FOREIGN KEY (`user_id`) REFERENCES `prm_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
        $this->execute("
            CREATE TABLE `prm_promise_work_post_comment` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `post_id` int(11) NOT NULL,
              `author_id` int(11) NOT NULL,
              `author_hidden` int(1) NOT NULL,
              `comment` text NOT NULL,
              `assessment` int(1) NOT NULL,
              `positive` int(11) NOT NULL,
              `negative` int(11) NOT NULL,
              `create_date` datetime NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_prm_promise_work_post_comment_1_idx` (`post_id`),
              KEY `fk_prm_promise_work_post_comment_2_idx` (`author_id`),
              CONSTRAINT `fk_prm_promise_work_post_comment_1` FOREIGN KEY (`post_id`) REFERENCES `prm_promise_work_comment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_promise_work_post_comment_2` FOREIGN KEY (`author_id`) REFERENCES `prm_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }
}
