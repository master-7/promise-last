<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_194147_add_default_user extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO prm_user
				(id, email, surname, `name`, password, salt)
					VALUES
				(1, 'master-7@inbox.ru', 'Администратор', 'Дефолтный',
				'38b47a29fa254c61041c9af5772b431ce400b160e808a694f15f13126123cbe7e5d1ffba0252a6938630cec2729c67f657ab3f1e8943921a20c2b72bb0a747e8',
				'674dafdfc32eb63d3e5e0e5b9ac1bbef14d3118e23bc3af0b53207a9430237ca5947ff54c1a64108aac3cd2362d79bfac6582bbc9bb0b9672f6f999bc43d7017');
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM prm_user
        ");
    }
}
