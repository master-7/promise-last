<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_124737_table_news extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_promise` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `user_id` INT(11) NOT NULL,
              `user_surname` VARCHAR(50) NOT NULL,
              `user_name` VARCHAR(50) NOT NULL,
              `image_path` VARCHAR(45) NOT NULL,
              `rate` INT NOT NULL,
              `title` VARCHAR(50) NOT NULL,
              `short_text` VARCHAR(255) NOT NULL,
              `description` TEXT NOT NULL,
              `success` VARCHAR(45) NOT NULL,
              `views` INT NOT NULL DEFAULT 0,
              `create_date` DATETIME NOT NULL,
              `start_date` DATETIME NOT NULL,
              `deadline` DATETIME NOT NULL,
              PRIMARY KEY (`id`),
              CONSTRAINT `fk_prm_promise_1`
              FOREIGN KEY (`user_id`)
              REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
              ENGINE = InnoDB CHARSET=UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM `prm_promise`
        ");
    }
}
