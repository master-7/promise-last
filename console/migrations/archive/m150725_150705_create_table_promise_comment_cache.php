<?php

use yii\db\Schema;
use yii\db\Migration;

class m150725_150705_create_table_promise_comment_cache extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_promise_cache` (
              `promise_id` INT NOT NULL,
              `json_data` LONGTEXT NOT NULL,
              PRIMARY KEY (`promise_id`),
              CONSTRAINT `fk_prm_comment_promise_cache_1`
                FOREIGN KEY (`promise_id`)
                REFERENCES `prm_promise` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB DEFAULT CHARSET=utf8
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS prm_promise_cache
        ");
    }
}
