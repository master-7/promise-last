<?php

use yii\db\Schema;
use yii\db\Migration;

class m150726_122315_change_table_user extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE prm_user ADD CONSTRAINT UNIQUE INDEX `uk_prm_user_access_token` (`access_token` ASC)
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_user DROP CONSTRAINT uk_prm_user_access_token
        ");
    }
}
