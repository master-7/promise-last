<?php

use yii\db\Schema;
use yii\db\Migration;

class m150720_090438_create_table_multimedia_gallery extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_multimedia_gallery` (
              `id` INT NOT NULL AUTO_INCREMENT,
              PRIMARY KEY (`id`))
            ENGINE = InnoDB CHARSET=UTF8
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_multimedia_gallery`
        ");
    }
}
