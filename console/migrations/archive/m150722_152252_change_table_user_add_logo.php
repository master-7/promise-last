<?php

use yii\db\Schema;
use yii\db\Migration;

class m150722_152252_change_table_user_add_logo extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `prm_user` ADD COLUMN `avatar` VARCHAR(255) DEFAULT NULL AFTER `name`
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE `prm_user` DROP COLUMN `avatar`
        ");
    }
}
