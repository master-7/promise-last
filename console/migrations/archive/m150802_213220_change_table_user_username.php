<?php

use yii\db\Schema;
use yii\db\Migration;

class m150802_213220_change_table_user_username extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE prm_user CHANGE email username varchar(128) NOT NULL
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_user CHANGE username email varchar(128) NOT NULL
        ");
    }
}
