<?php

use yii\db\Schema;
use yii\db\Migration;

class m150720_090657_create_table_work_promise extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_promise_work` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `promise_id` INT NOT NULL,
              `logo` VARCHAR(255) NOT NULL,
              `title` VARCHAR(255) NOT NULL,
              `description` MEDIUMTEXT NOT NULL,
              `multimedia` INT NOT NULL DEFAULT 0,
              `create_date` DATETIME NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `fk_prm_promise_work_1_idx` (`promise_id` ASC),
              INDEX `fk_prm_promise_work_2_idx` (`multimedia` ASC),
              CONSTRAINT `fk_prm_promise_work_1`
              FOREIGN KEY (`promise_id`)
              REFERENCES `prm_promise` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_promise_work_2`
              FOREIGN KEY (`multimedia`)
              REFERENCES `prm_multimedia_gallery` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
              ENGINE = InnoDB CHARSET=UTF8
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_promise_work`
        ");
    }
}
