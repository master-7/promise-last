<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_070957_menu extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `pr_menu` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `title` VARCHAR(255) NOT NULL,
              `icon` VARCHAR(255) NOT NULL,
              `link` VARCHAR(255) NOT NULL,
              PRIMARY KEY (`id`)
			)
            ENGINE = InnoDB DEFAULT CHARSET=UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `pr_menu`
        ");
    }
}
