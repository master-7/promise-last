<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_045003_change_promise_add_is_block extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE prm_promise ADD COLUMN is_block INT NOT NULL DEFAULT 0 AFTER deadline
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_promise DROP COLUMN is_block
        ");
    }
}
