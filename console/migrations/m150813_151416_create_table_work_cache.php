<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_151416_create_table_work_cache extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_promise_work_cache` (
              `promise_id` INT NOT NULL,
              `work_id` INT NOT NULL,
              `json_data` LONGTEXT NULL,
              PRIMARY KEY (`promise_id`, `work_id`),
              INDEX `fk_promise_work_cache_2_idx` (`work_id` ASC),
              CONSTRAINT `fk_promise_work_cache_1`
                FOREIGN KEY (`promise_id`)
                REFERENCES `prm_promise` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_promise_work_cache_2`
                FOREIGN KEY (`work_id`)
                REFERENCES `prm_promise_work` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB DEFAULT CHARSET UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_promise_work_cache`
        ");
    }
}
