<?php

use yii\db\Schema;
use yii\db\Migration;

class m150814_103806_alter_promise_work_entity extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE prm_promise_work
              ADD COLUMN user_id INT AFTER promise_id
        ");

        $this->execute("
            ALTER TABLE prm_promise_work ADD
              CONSTRAINT fk_prm_promise_work_to_prm_user
              FOREIGN KEY (user_id)
              REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION
        ");

        $this->execute("
            ALTER TABLE prm_promise_work_cache
              ADD COLUMN user_id INT AFTER work_id
        ");

        $this->execute("
            ALTER TABLE prm_promise_work_cache ADD
              CONSTRAINT fk_prm_promise_work_cache_to_prm_user
              FOREIGN KEY (user_id)
              REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_promise_work
              DROP FOREIGN KEY fk_prm_promise_work_to_prm_user
        ");

        $this->execute("
            ALTER TABLE prm_promise_work
              DROP COLUMN user_id
        ");

        $this->execute("
            ALTER TABLE prm_promise_work_cache
              DROP FOREIGN KEY fk_prm_promise_work_cache_to_prm_user
        ");

        $this->execute("
            ALTER TABLE prm_promise_work_cache
              DROP COLUMN user_id
        ");
    }
}
