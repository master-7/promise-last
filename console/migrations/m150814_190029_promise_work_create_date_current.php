<?php

use yii\db\Schema;
use yii\db\Migration;

class m150814_190029_promise_work_create_date_current extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE prm_promise_work
              CHANGE create_date create_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_promise_work
              CHANGE create_date create_date DATETIME NOT NULL
        ");
    }
}
