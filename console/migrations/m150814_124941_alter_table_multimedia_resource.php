<?php

use yii\db\Schema;
use yii\db\Migration;

class m150814_124941_alter_table_multimedia_resource extends Migration
{
    public function safeUp()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_multimedia_resource`
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_multimedia_resource` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `gallery_id` INT NOT NULL,
              `link` VARCHAR(255) NOT NULL,
              `description` VARCHAR(255) NOT NULL,
              `type` INT(1) NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `fk_prm_multimedia_resource_1_idx` (`gallery_id` ASC),
              CONSTRAINT `fk_prm_multimedia_resource_1`
                FOREIGN KEY (`gallery_id`)
                REFERENCES `prm_multimedia_gallery` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB DEFAULT CHARSET=utf8
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_multimedia_resource`
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_multimedia_resource` (
              `gallery_id` int(11) NOT NULL,
              `link` varchar(255) NOT NULL,
              `description` varchar(255) NOT NULL,
              `type` int(1) NOT NULL COMMENT '1 - Photo, 2 - Video',
              PRIMARY KEY (`gallery_id`),
              CONSTRAINT `fk_prm_multimedia_resource_1`
                FOREIGN KEY (`gallery_id`)
                REFERENCES `prm_multimedia_gallery` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }
}
