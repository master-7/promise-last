<?php

use yii\db\Schema;
use yii\db\Migration;

class m150814_090331_create_table_promise_work_comment_assessment extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_promise_work_comment_assessment` (
              `work_comment_id` INT NOT NULL,
              `user_id` INT NOT NULL,
              `assessment` INT(1) NOT NULL,
              PRIMARY KEY (`work_comment_id`, `user_id`),
              INDEX `fk_prm_promise_work_comment_assessment_2_idx` (`user_id` ASC),
              CONSTRAINT `fk_prm_promise_work_comment_assessment_1`
                FOREIGN KEY (`work_comment_id`)
                REFERENCES `prm_promise_work_comment` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_promise_work_comment_assessment_2`
                FOREIGN KEY (`user_id`)
                REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB DEFAULT CHARSET UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_promise_work_comment_assessment`
        ");
    }
}
