<?php

use yii\db\Schema;
use yii\db\Migration;

class m150810_071537_add_category extends Migration
{
    public function safeUp()
    {
        $this->execute("
            SET SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO';
        ");

        $this->execute("
            INSERT INTO prm_category (id, name, active)
              VALUES
            (1, 'Спорт', 1),
            (2, 'Музыка', 1),
            (3, 'Финансы', 1),
            (4, 'Семья', 1),
            (5, 'Отдых и развлечения', 1),
            (6, 'Самосовершенствование', 1),
            (7, 'Работа и карьера', 1),
            (8, 'Бизнес', 1),
            (9, 'Учеба', 1),
            (10, 'Наука и техника', 1),
            (11, 'Здоровье и красота', 1),
            (0, 'Другое', 1)
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM prm_category
        ");
    }
}
