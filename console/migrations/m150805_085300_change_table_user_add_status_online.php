<?php

use yii\db\Schema;
use yii\db\Migration;

class m150805_085300_change_table_user_add_status_online extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE prm_user CHANGE patronymic online INT(1) NOT NULL DEFAULT 0
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_user CHANGE online patronymic varchar(50) DEFAULT NULL
        ");
    }
}
