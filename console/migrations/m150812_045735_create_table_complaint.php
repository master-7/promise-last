<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_045735_create_table_complaint extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_complaint_type` (
              `id` INT NOT NULL,
              `type` VARCHAR(45) NOT NULL,
              PRIMARY KEY (`id`))
            ENGINE = InnoDB DEFAULT CHARSET UTF8;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_complaint` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `promise_id` INT NOT NULL,
              `type` INT NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `fk_prm_complaint_1_idx` (`promise_id` ASC),
              INDEX `fk_prm_complaint_2_idx` (`type` ASC),
              CONSTRAINT `fk_prm_complaint_1`
                FOREIGN KEY (`promise_id`)
                REFERENCES `prm_promise` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_complaint_2`
                FOREIGN KEY (`type`)
                REFERENCES `prm_complaint_type` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB DEFAULT CHARSET UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_complaint_type`
        ");

        $this->execute("
            DROP TABLE IF EXISTS `prm_complaint`
        ");
    }
}
