<?php

use yii\db\Schema;
use yii\db\Migration;

class m150818_204219_alter_table_multimedia_resource extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE prm_multimedia_resource
              CHANGE description description varchar(255)
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_multimedia_resource
              CHANGE description description varchar(255) NOT NULL
        ");
    }
}
