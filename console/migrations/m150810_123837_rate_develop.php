<?php

use yii\db\Schema;
use yii\db\Migration;

class m150810_123837_rate_develop extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE prm_promise CHANGE rate rate TEXT NOT NULL
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_promise_rate_type` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(45) NOT NULL,
              PRIMARY KEY (`id`))
            ENGINE = InnoDB DEFAULT CHARSET UTF8;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_promise_rate` (
              `promise_id` INT NOT NULL,
              `type_rate` INT NOT NULL,
              `rate` TEXT NOT NULL,
              PRIMARY KEY (`promise_id`),
              INDEX `fk_prm_promise_rate_2_idx` (`type_rate` ASC),
              CONSTRAINT `fk_prm_promise_rate_1`
                FOREIGN KEY (`promise_id`)
                REFERENCES `prm_promise` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_promise_rate_2`
                FOREIGN KEY (`type_rate`)
                REFERENCES `prm_promise_rate_type` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB DEFAULT CHARSET UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_promise CHANGE rate rate int(11) NOT NULL
        ");

        $this->execute("
            DROP TABLE IF EXISTS `prm_promise_rate`;
        ");
        
        $this->execute("
            DROP TABLE IF EXISTS `prm_promise_rate_type`;
        ");
    }
}
