<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_183631_create_table_prm_multimedia_resource extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_multimedia_resource` (
              `gallery_id` INT NOT NULL,
              `link` VARCHAR(255) NOT NULL,
              `description` VARCHAR(255) NOT NULL,
              `type` INT(1) NOT NULL COMMENT '1 - Photo, 2 - Video',
              PRIMARY KEY (`gallery_id`),
              CONSTRAINT `fk_prm_multimedia_resource_1`
                FOREIGN KEY (`gallery_id`)
                REFERENCES `prm_multimedia_gallery` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB DEFAULT CHARSET UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_multimedia_resource` 
        ");
    }
}
