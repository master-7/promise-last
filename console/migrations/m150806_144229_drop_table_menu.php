<?php

use yii\db\Schema;
use yii\db\Migration;

class m150806_144229_drop_table_menu extends Migration
{
    public function safeUp()
    {
        $this->execute("
            DROP TABLE IF EXISTS pr_menu
        ");
    }

    public function safeDown()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `pr_menu` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `title` varchar(255) NOT NULL,
              `icon` varchar(255) NOT NULL,
              `link` varchar(255) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
        $this->execute("
            INSERT INTO pr_menu (id, title, icon, link)
              VALUES
                (1, 'Новое', 'what-shot', 'news'),
                (2, 'Избранное', 'bookmark', 'favorites');
        ");
    }
}
