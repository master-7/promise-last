<?php

use yii\db\Schema;
use yii\db\Migration;

class m150811_100411_change_table_promise extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE prm_promise CHANGE create_date create_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
        ");

        $this->execute("
            ALTER TABLE prm_promise DROP COLUMN start_date
        ");

        $this->execute("
            ALTER TABLE prm_promise CHANGE success success varchar(45)
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_promise CHANGE create_date create_date datetime NOT NULL
        ");

        $this->execute("
            ALTER TABLE prm_promise ADD COLUMN start_date datetime NOT NULL AFTER create_date
        ");

        $this->execute("
            ALTER TABLE prm_promise CHANGE success success varchar(45) NOT NULL
        ");
    }
}
