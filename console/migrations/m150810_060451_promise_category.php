<?php

use yii\db\Schema;
use yii\db\Migration;

class m150810_060451_promise_category extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_category` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(45) NOT NULL,
              `active` INT NOT NULL DEFAULT 0,
              PRIMARY KEY (`id`))
            ENGINE = InnoDB DEFAULT CHARSET UTF8;
        ");

        $this->execute("
            ALTER TABLE prm_promise ADD COLUMN category_id INT AFTER user_id
        ");

        $this->execute("
            ALTER TABLE prm_promise ADD
              CONSTRAINT fk_prm_promise_to_prm_category
              FOREIGN KEY (category_id)
              REFERENCES `prm_category` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_promise DROP FOREIGN KEY fk_prm_promise_to_prm_category
        ");

        $this->execute("
            ALTER TABLE prm_promise DROP COLUMN category_id
        ");

        $this->execute("
            DROP TABLE IF EXISTS `prm_category`
        ");
    }
}
