<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_094135_user_promise_cache extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE prm_promise_cache
              ADD COLUMN user_id int(11) NOT NULL DEFAULT 0 AFTER promise_id
        ");

        $this->execute("
            ALTER TABLE prm_promise_cache ADD
              CONSTRAINT fk_prm_promise_cache_user_id_to_prm_user_id
              FOREIGN KEY (user_id)
              REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE prm_promise_cache DROP COLUMN user_id
        ");

        $this->execute("
            ALTER TABLE prm_promise_cache DROP FOREIGN KEY fk_prm_promise_cache_user_id_to_prm_user_id
        ");
    }
}
