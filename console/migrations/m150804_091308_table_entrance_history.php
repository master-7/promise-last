<?php

use yii\db\Schema;
use yii\db\Migration;

class m150804_091308_table_entrance_history extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_entrance_history` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `user_id` INT NOT NULL,
              `ip_address` VARCHAR(45) NOT NULL,
              `date_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              `status` INT(1) NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `fk_prm_entrance_history_1_idx` (`user_id` ASC),
              CONSTRAINT `fk_prm_entrance_history_1`
              FOREIGN KEY (`user_id`)
              REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
              ENGINE = InnoDB DEFAULT CHARSET UTF8
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_entrance_history`
        ");
    }
}
