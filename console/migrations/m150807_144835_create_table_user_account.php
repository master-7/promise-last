<?php

use yii\db\Schema;
use yii\db\Migration;

class m150807_144835_create_table_user_account extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_user_account` (
              `user_id` INT NOT NULL,
              `balance` INT NOT NULL,
              PRIMARY KEY (`user_id`),
              CONSTRAINT `fk_prm_user_account_1`
                FOREIGN KEY (`user_id`)
                REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB DEFAULT CHARSET UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_user_account`
        ");
    }
}
