<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_144854_create_table_promise_work_comment extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `prm_promise_work_comment` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `promise_work_id` INT NOT NULL,
              `author_id` INT NOT NULL,
              `post_id` INT NULL,
              `author_hidden` INT(1) NOT NULL,
              `comment` TEXT NOT NULL,
              `positive` INT NOT NULL,
              `negative` INT NOT NULL,
              `status` INT(1) NOT NULL DEFAULT 1,
              `create_date` DATETIME NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `fk_prm_promise_work_comment_1_idx` (`promise_work_id` ASC),
              INDEX `fk_prm_promise_work_comment_2_idx` (`author_id` ASC),
              INDEX `fk_prm_promise_work_comment_3_idx` (`post_id` ASC),
              CONSTRAINT `fk_prm_promise_work_comment_1`
                FOREIGN KEY (`promise_work_id`)
                REFERENCES `prm_promise_work` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_promise_work_comment_2`
                FOREIGN KEY (`author_id`)
                REFERENCES `prm_user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_prm_promise_work_comment_3`
                FOREIGN KEY (`post_id`)
                REFERENCES `prm_promise_work_comment` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB DEFAULT CHARSET UTF8;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DROP TABLE IF EXISTS `prm_promise_work_comment`
        ");
    }
}
