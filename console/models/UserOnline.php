<?php

namespace console\models;

use Yii;
use common\models\User;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class UserOnline implements MessageComponentInterface {

    /**
     * User status constant
     */
    const USER_OFFLINE = 0;
    const USER_ONLINE = 1;

    /**
     * Save the connections
     * @var \SplObjectStorage
     */
    protected $clients;

    /**
     * Count clients the connect by username
     * @var
     */
    protected $countClientsByUsername;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $username) {
        //Save client connections
        $this->clients->offsetSet($from, $username);

        $username = preg_replace('/\\r\\n$/', '', $username);
        echo "New user online $username \n";

        self::setUserStatus($username, self::USER_ONLINE);
    }

    public function onClose(ConnectionInterface $conn) {
        echo "Close connection! ({$conn->resourceId})\n";

        $username = $this->clients->offsetGet($conn);
        if($username) {
            //Set status offline
            echo "User offline $username \n";

            self::setUserStatus($username, self::USER_OFFLINE);
        }
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $username = $this->clients->offsetGet($conn);
        if($username) {
            //Set status offline
            echo "User offline $username \n";

            self::setUserStatus($username, self::USER_OFFLINE);

            echo "An error has occurred: {$e->getMessage()}\n";

            $conn->close();
        }
    }

    /**
     * Function set the user status by dependencies of the current conditions
     * @param $username
     * @param $status
     */
    public function setUserStatus($username, $status)
    {
        //Flag the user change status
        $setStatus = false;

        if($status == self::USER_ONLINE) {
            if(isset($this->countClientsByUsername[$username]))
                $this->countClientsByUsername[$username]++;
            else
                $this->countClientsByUsername[$username] = 1;

            //Set status online if the first client by this username
            if($this->countClientsByUsername[$username] == 1)
                $setStatus = true;
        } else {
            $this->countClientsByUsername[$username]--;

            //Set status offline if the last connections
            if($this->countClientsByUsername[$username] < 1)
                $setStatus = true;
        }

        if($setStatus) {
            $model = User::findByUsername($username);

            if ($model) {
                $model->password = null;
                $model->online = $status;
                $model->save();
            }
        }
    }

}