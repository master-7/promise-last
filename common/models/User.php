<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $surname
 * @property string $name
 * @property string $online
 * @property string $avatar
 * @property integer $sex
 * @property integer $city_id
 * @property string $password write-only password
 * @property string $salt
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $access_token
 * @property integer $status
 * @property integer $assessment
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $create_date
 * @property string $edit_date
 *
 * @property UserAccount $account
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * Sex constant
     */
    const SEX_UNKNOWN = 0;
    const SEX_MALE = 1;
    const SEX_FEMALE = 2;

    /**
     * Validate constant
     */
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_user';
    }

    /**
     * @inheritdoc
     */
    /*
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'name', 'surname'], 'required'],
            ['username', 'email'],
            ['username', 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => _('ID'),
            'name' => _('Имя'),
            'surname' => _('Фамилия'),
            'online' => _('Онлайн'),
            'password' => _('Пароль'),
            'salt' => _('Соль'),
            'auth_key' => _('Auth Key'),
            'access_token' => _('Access Token'),
        ];
    }

    /**
     * Before save event handler
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($this->getIsNewRecord() && !empty($this->password))
                $this->salt = $this->saltGenerator();
            if(!empty($this->password))
                $this->password = $this->passWithSalt($this->password, $this->salt);
            else
                unset($this->password);
            return true;
        } else
            return false;
    }

    /**
     * Generate the salt
     * @return string
     */
    public function saltGenerator()
    {
        return hash("sha512", uniqid('salt_', true));
    }

    /**
     * Generate the access token
     * @return string
     */
    public function accessTokenGenerator()
    {
        return hash("sha512", uniqid('token_', true));
    }

    /**
     * Return pass with the salt
     * @param $password
     * @param $salt
     * @return string
     */
    public function passWithSalt($password, $salt)
    {
        return hash("sha512", $password.$salt);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey()["id"];
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $this->passWithSalt($password, $this->salt);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $this->passWithSalt($password, $this->saltGenerator());
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(UserAccount::className(), ['user_id' => 'id']);
    }

    /**
     * Return the client info for application
     * @return array
     */
    public function getClientInfo()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'online' => $this->online,
            'username' => $this->username,
            'avatar' => $this->avatar,
            'sex' => $this->sex,
            'status' => $this->status,
            'rating' => $this->assessment,
            'balanceAccount' => isset($this->account->balance) ? $this->account->balance : null
        ];
    }
}
