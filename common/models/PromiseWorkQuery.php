<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PromiseWork]].
 *
 * @see PromiseWork
 */
class PromiseWorkQuery extends \yii\db\ActiveQuery
{
    /**
     * Add condition with id
     * @param $id
     * @return $this
     */
    public function withPk($id)
    {
        $this->andWhere(
            'prm_promise_work.id = :id',
            [
                ':id' => $id
            ]
        );
        return $this;
    }

    /**
     * Add condition with promise id
     * @param $promise_id
     * @return $this
     */
    public function withPromise($promise_id)
    {
        $this->andWhere(
            'prm_promise_work.promise_id = :promise_id',
            [
                ':promise_id' => $promise_id
            ]
        );
        return $this;
    }

    /**
     * Add condition with promise id
     * @param $user_id
     * @return $this
     */
    public function withUser($user_id)
    {
        $this->andWhere(
            'prm_promise_work.user_id = :user_id',
            [
                ':user_id' => $user_id
            ]
        );
        return $this;
    }

    /**
     * @inheritdoc
     * @return PromiseWork[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PromiseWork|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}