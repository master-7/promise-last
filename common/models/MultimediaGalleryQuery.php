<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[MultimediaGallery]].
 *
 * @see MultimediaGallery
 */
class MultimediaGalleryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MultimediaGallery[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MultimediaGallery|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}