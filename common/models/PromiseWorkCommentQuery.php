<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PromiseWorkComment]].
 *
 * @see PromiseWorkComment
 */
class PromiseWorkCommentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PromiseWorkComment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PromiseWorkComment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}