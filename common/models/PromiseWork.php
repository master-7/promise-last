<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_promise_work".
 *
 * @property integer $id
 * @property integer $promise_id
 * @property integer $user_id
 * @property string $logo
 * @property string $title
 * @property string $description
 * @property integer $multimedia
 * @property string $create_date
 *
 * @property Promise $promise
 * @property MultimediaGallery $multimediaGallery
 * @property MultimediaResource $multimediaResource
 * @property User $user
 * @property PromiseWorkCache[] $promiseWorkCaches
 * @property Promise[] $promises
 * @property PromiseWorkComment[] $promiseWorkComments
 */
class PromiseWork extends \yii\db\ActiveRecord
{
    /**
     * Validate constants
     */
    const MAX_LENGTH_LOGO_AND_TITLE = 255;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_promise_work';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promise_id', 'logo', 'title', 'description'], 'required'],
            [['promise_id', 'user_id', 'multimedia'], 'integer'],
            [['description'], 'string'],
            [['create_date'], 'safe'],
            [['logo', 'title'], 'string', 'max' => self::MAX_LENGTH_LOGO_AND_TITLE],
            [['promise_id'], 'exist',
                'targetClass' => Promise::className(), 'targetAttribute' => ['promise_id' => 'id']],
            [['multimedia'], 'exist',
                'targetClass' => MultimediaGallery::className(), 'targetAttribute' => ['multimedia' => 'id']],
            [['user_id'], 'exist',
                'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'promise_id' => Yii::t('app', 'Promise ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'logo' => Yii::t('app', 'Logo'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'multimedia' => Yii::t('app', 'Multimedia'),
            'create_date' => Yii::t('app', 'Create Date'),
        ];
    }

    /**
     * After save recount cache comments
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        PromiseCache::doPromiseCache($this->promise_id);
        PromiseWorkCache::doPromiseWorkCache($this->id);

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromise()
    {
        return $this->hasOne(Promise::className(), ['id' => 'promise_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMultimediaGallery()
    {
        return $this->hasOne(MultimediaGallery::className(), ['id' => 'multimedia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMultimediaResource()
    {
        return $this->hasMany(MultimediaResource::className(), ['gallery_id' => 'multimedia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromiseWorkCaches()
    {
        return $this->hasMany(PromiseWorkCache::className(), ['work_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromises()
    {
        return $this->hasMany(Promise::className(), ['id' => 'promise_id'])
            ->viaTable('prm_promise_work_cache', ['work_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromiseWorkComments()
    {
        return $this->hasMany(PromiseWorkComment::className(), ['promise_work_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return PromiseWorkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PromiseWorkQuery(get_called_class());
    }
}
