<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[CommentPromise]].
 *
 * @see CommentPromise
 */
class CommentPromiseQuery extends \yii\db\ActiveQuery
{
    /**
     * Add condition with news_id
     * @param $id_news
     * @return $this
     */
    public function withNews($id_news)
    {
        $this->andWhere(
            'prm_comment_promise.news_id = :news_id', [':news_id' => $id_news]
        );
        return $this;
    }

    /**
     * @inheritdoc
     * @return CommentPromise[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CommentPromise|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}