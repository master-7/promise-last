<?php

namespace common\models;
use Faker\Provider\cs_CZ\DateTime;

/**
 * This is the ActiveQuery class for [[EntranceHistory]].
 *
 * @see EntranceHistory
 */
class EntranceHistoryQuery extends \yii\db\ActiveQuery
{
    /**
     * Count fail authenticate by past hour
     * @return $this
     */
    public function countFailByPastHour()
    {
        //Past hour time
        $now = new \DateTime();
        $pastHour = $now->sub(new \DateInterval('PT1H'));

        $this->select(['COUNT(*) AS countFail'])
            ->andWhere(
                'status = :status',
                [
                    ':status' => EntranceHistory::STATUS_FAIL
                ]
            )
            ->andWhere(
                'date_time > :datetime',
                [
                    ':datetime' => $pastHour->format('Y-m-d H:i:s')
                ]
            )
            ->groupBy('user_id');
        return $this;
    }

    /**
     * Common count authenticate by past hour
     * @return $this
     */
    public function countByPastHour()
    {
        //Past hour time
        $now = new \DateTime();
        $pastHour = $now->sub(new \DateInterval('PT1H'));

        $this->select(['COUNT(*) AS countFail'])
            ->andWhere(
                'date_time > :datetime',
                [
                    ':datetime' => $pastHour->format('Y-m-d H:i:s')
                ]
            )
            ->groupBy('user_id');
        return $this;
    }

    /**
     * @inheritdoc
     * @return EntranceHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EntranceHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}