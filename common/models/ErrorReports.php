<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_error_reports".
 *
 * @property integer $id
 * @property string $comment
 * @property string $value
 * @property string $create_date
 * @property string $ip_address
 */
class ErrorReports extends \yii\db\ActiveRecord
{
    /**
     * Validate constant
     */
    const MAX_LENGTH_IP_ADDRESS = 45;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_error_reports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment', 'value', 'ip_address'], 'required'],
            [['comment', 'value'], 'string'],
            [['create_date'], 'safe'],
            [['ip_address'], 'string', 'max' => self::MAX_LENGTH_IP_ADDRESS],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'comment' => Yii::t('app', 'Comment'),
            'value' => Yii::t('app', 'Value'),
            'create_date' => Yii::t('app', 'Create Date'),
            'ip_address' => Yii::t('app', 'Ip Address'),
        ];
    }
}
