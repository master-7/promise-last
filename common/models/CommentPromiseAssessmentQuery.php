<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[CommentPromiseAssessment]].
 *
 * @see CommentPromiseAssessment
 */
class CommentPromiseAssessmentQuery extends \yii\db\ActiveQuery
{
    /**
     * Get comment rating as a sum(assessment)
     * @return $this
     */
    public function getCommentRating()
    {
        $this->addSelect("(sum(prm_comment_promise_assessment.assessment)) AS rating");
        $this->groupBy(['prm_comment_promise_assessment.comment_promise']);

        return $this;
    }

    /**
     * Add condition comment_promise
     * @param $comment_promise_id
     * @return $this
     */
    public function withCommentPromise($comment_promise_id)
    {
        $this->andWhere(
            'prm_comment_promise_assessment.comment_promise = :comment_promise_id',
            [
                ':comment_promise_id' => $comment_promise_id
            ]
        );
        return $this;
    }

    /**
     * Add condition user_id
     * @param $user_id
     * @return $this
     */
    public function withUser($user_id)
    {
        $this->andWhere(
            'prm_comment_promise_assessment.user_id = :user_id',
            [
                ':user_id' => $user_id
            ]
        );
        return $this;
    }

    /**
     * @inheritdoc
     * @return CommentPromiseAssessment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CommentPromiseAssessment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}