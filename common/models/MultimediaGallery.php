<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_multimedia_gallery".
 *
 * @property integer $id
 *
 * @property PromiseWork[] $promiseWorks
 */
class MultimediaGallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_multimedia_gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromiseWorks()
    {
        return $this->hasMany(PromiseWork::className(), ['multimedia' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMultimediaResource()
    {
        return $this->hasMany(MultimediaResource::className(), ['multimedia' => 'id']);
    }

    /**
     * @inheritdoc
     * @return MultimediaGalleryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MultimediaGalleryQuery(get_called_class());
    }
}
