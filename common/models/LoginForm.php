<?php
namespace common\models;

use Yii;
use yii\base\Model;
use \yii\web\Cookie;

/**
 * Login form
 */
class LoginForm extends Model
{
    /**
     * Count the save days user
     */
    const SAVE_DAYS = 180;

    public $username;
    public $password;
    public $rememberMe = true;
    public $countFailAttempt;

    private $_user = false;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // Max count attempt the entrance
            ['countFailAttempt', 'number', 'max' => EntranceHistory::MAX_COUNT_FAIL],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * Entrance form save datetime the success or fail authenticate
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        $countFail = EntranceHistory::find()->countFailByPastHour()->asArray()->one();

        $this->countFailAttempt = isset($countFail['countFail']) ? $countFail['countFail'] : 0;

        $entranceModel = new EntranceHistory();
        $entranceModel->user_id = $this->getUser()->id;
        $entranceModel->ip_address = Yii::$app->getRequest()->getUserIP();

        if ($this->validate()) {
            $entranceModel->status = EntranceHistory::STATUS_SUCCESS;
            $entranceModel->save();
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * self::SAVE_DAYS : 0);
        } else {
            $entranceModel->status = EntranceHistory::STATUS_FAIL;
            $entranceModel->save();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function loginAndAuthKey()
    {
        if ($this->validate()) {
            $user = User::findByUsername($this->username);

            $user->access_token = $user->accessTokenGenerator();
            $user->password = null;
            $user->save();

            $cookies = Yii::$app->response->cookies;
            $cookies->remove('authKey');

            $cookies->add(new Cookie([
                'name' => 'authKey',
                'value' => $user->access_token,
                'httpOnly' => false
            ]));
            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
