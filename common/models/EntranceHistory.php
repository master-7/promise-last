<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_entrance_history".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $ip_address
 * @property string $date_time
 * @property integer $status
 *
 * @property User $user
 */
class EntranceHistory extends \yii\db\ActiveRecord
{
    /**
     * The status of entrance
     */
    const STATUS_FAIL = 0;
    const STATUS_SUCCESS = 1;

    /**
     * Max fail attempt
     */
    const MAX_COUNT_FAIL = 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_entrance_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ip_address', 'status'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['date_time'], 'safe'],
            [['ip_address'], 'string', 'max' => 45],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'ip_address' => Yii::t('app', 'Ip Address'),
            'date_time' => Yii::t('app', 'Date Time'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return EntranceHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EntranceHistoryQuery(get_called_class());
    }
}
