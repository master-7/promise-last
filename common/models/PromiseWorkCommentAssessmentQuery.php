<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PromiseWorkCommentAssessment]].
 *
 * @see PromiseWorkCommentAssessment
 */
class PromiseWorkCommentAssessmentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PromiseWorkCommentAssessment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PromiseWorkCommentAssessment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}