<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_complaint_type".
 *
 * @property integer $id
 * @property string $type
 *
 * @property Complaint[] $complaints
 */
class ComplaintType extends \yii\db\ActiveRecord
{
    /**
     * Validate constant
     */
    const MAX_LENGTH_TYPE = 45;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_complaint_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type'], 'required'],
            [['id'], 'integer'],
            [['type'], 'string', 'max' => self::MAX_LENGTH_TYPE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplaints()
    {
        return $this->hasMany(Complaint::className(), ['type' => 'id']);
    }
}
