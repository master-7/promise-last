<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_promise_assessment".
 *
 * @property integer $promise_id
 * @property integer $user_id
 * @property integer $assessment
 * @property integer $active
 * @property string $create_date
 *
 * @property User $user
 * @property Promise $promise
 */
class PromiseAssessment extends \yii\db\ActiveRecord
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_promise_assessment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promise_id', 'user_id', 'assessment'], 'required'],
            [['promise_id', 'user_id', 'assessment'], 'integer'],
            [['create_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'promise_id' => Yii::t('app', 'Promise ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'assessment' => Yii::t('app', 'Assessment'),
            'create_date' => Yii::t('app', 'Create Date'),
        ];
    }

    /**
     * On/Off assessment users join or create commentAssessment
     * @param $promiseId
     * @param $status
     */
    public static function assessmentsOnOff($promiseId, $status)
    {
        $usersJoin = CommentPromiseAssessment::find()
            ->withCommentPromise($promiseId)
            ->asArray()
            ->all();
        foreach($usersJoin as $key => $userJoin) {
            $promiseAssessment = PromiseAssessment::find()
                ->withPromise($promiseId)
                ->withUser($userJoin['user_id'])
                ->one();
            $promiseAssessment->active = $status;
            $promiseAssessment->save();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromise()
    {
        return $this->hasOne(Promise::className(), ['id' => 'promise_id']);
    }

    /**
     * @inheritdoc
     * @return PromiseAssessmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PromiseAssessmentQuery(get_called_class());
    }
}
