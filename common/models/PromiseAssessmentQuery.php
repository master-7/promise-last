<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PromiseAssessment]].
 *
 * @see PromiseAssessment
 */
class PromiseAssessmentQuery extends \yii\db\ActiveQuery
{
    /**
     * Get active assessment as a rating in a percent
     * @return $this
     */
    public function getPromiseRating()
    {
        $this->addSelect("(count(prm_promise_assessment.assessment)
                                /
                        sum(prm_promise_assessment.assessment)) * 100 AS rating");
        $this->groupBy(['promise_id']);

        $this->andWhere(
            'prm_promise_assessment.active = :active',
            [
                ':active' => PromiseAssessment::STATUS_ACTIVE
            ]
        );

        return $this;
    }

    /**
     * Get promise active sum(assessment)
     * @return $this
     */
    public function getPromiseAssessment()
    {
        $this->addSelect("sum(prm_promise_assessment.assessment) AS rating");

        $this->groupBy(['promise_id']);

        $this->andWhere(
            'prm_promise_assessment.active = :active',
            [
                ':active' => PromiseAssessment::STATUS_ACTIVE
            ]
        );

        return $this;
    }

    /**
     * Add condition with user_id
     * @param $user_id
     * @return $this
     */
    public function withUser($user_id)
    {
        $this->andWhere(
            'prm_promise_assessment.user_id = :user_id',
            [
                ':user_id' => $user_id
            ]
        );
        return $this;
    }

    /**
     * Add condition with promise_id
     * @param $promise_id
     * @return $this
     */
    public function withPromise($promise_id)
    {
        $this->andWhere(
            'prm_promise_assessment.promise_id = :promise_id',
            [
                ':promise_id' => $promise_id
            ]
        );
        return $this;
    }

    /**
     * @inheritdoc
     * @return PromiseAssessment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PromiseAssessment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}