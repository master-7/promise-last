<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PromiseRate]].
 *
 * @see PromiseRate
 */
class PromiseRateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PromiseRate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PromiseRate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}