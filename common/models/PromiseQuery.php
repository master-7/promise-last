<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Promise]].
 *
 * @see Promise
 */
class PromiseQuery extends \yii\db\ActiveQuery
{
    /**
     * Add condition with id
     * @param $id
     * @return $this
     */
    public function withPk($id)
    {
        $this->andWhere(
            'prm_promise.id = :id',
            [
                ':id' => $id
            ]
        );
        return $this;
    }

    /**
     * Condition returned last news less then variable $id and limited variable $limit
     * Usages in a infinity scroll
     * @param $id
     * @param $limit
     * @return $this
     */
    public function lessThenValueAndLimit($id, $limit)
    {
        $idStart = $id - $limit;
        $this->andWhere(
            'prm_promise.id >= :idStart AND prm_promise.id < :idFinish',
            [
                ':idStart' => $idStart,
                ':idFinish' => $id
            ]
        );
        return $this;
    }

    /**
     * Condition the user owner promise
     * @param $user_id
     * @return $this
     */
    public function withUserOwner($user_id)
    {
        $this->andWhere(
            'prm_promise.user_id = :user_id',
            [
                ':user_id' => $user_id
            ]
        );
        return $this;
    }

    /**
     * @inheritdoc
     * @return Promise[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Promise|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}