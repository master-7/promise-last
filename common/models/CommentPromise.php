<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_comment_promise".
 *
 * @property integer $id
 * @property integer $news_id
 * @property integer $author_id
 * @property integer $post_id
 * @property integer $author_hidden
 * @property string $comment
 * @property integer $assessment
 * @property integer $positive
 * @property integer $negative
 * @property integer $status
 * @property string $create_date
 *
 * @property Promise $news
 * @property User $author
 */
class CommentPromise extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    const AUTHOR_NOT_HIDDEN = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_comment_promise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'author_id', 'author_hidden', 'comment', 'assessment', 'positive', 'negative', 'create_date'], 'required'],
            [['news_id', 'author_id', 'author_hidden', 'assessment', 'positive', 'negative'], 'integer'],
            [['comment'], 'string'],
            [['create_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'news_id' => Yii::t('app', 'News ID'),
            'author_id' => Yii::t('app', 'Author ID'),
            'author_hidden' => Yii::t('app', 'Author Hidden'),
            'comment' => Yii::t('app', 'Comment'),
            'assessment' => Yii::t('app', 'Assessment'),
            'positive' => Yii::t('app', 'Positive'),
            'negative' => Yii::t('app', 'Negative'),
            'create_date' => Yii::t('app', 'Create Date'),
        ];
    }

    /**
     * Before save need add assessment to dependents model
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if($this->getIsNewRecord()) {
            //Check assessment in a exists and save if not exists
            $notExistsAssessment = !PromiseAssessment::find()->withUser($this->author_id)->exists();
            if($notExistsAssessment) {
                $promiseAssessment = new PromiseAssessment();
                $promiseAssessment->promise_id = $this->news_id;
                $promiseAssessment->user_id = $this->author_id;
                $promiseAssessment->assessment = $this->assessment;
                if($promiseAssessment->validate())
                    $promiseAssessment->save();
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * After save recount rating promise and cache comments
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        Promise::recountRating($this->news_id);

        PromiseCache::doPromiseCache($this->news_id);

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(Promise::className(), ['id' => 'news_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id'])
            ->from(['author_comment_promise' => 'prm_user'])
            ->select([
                'id',
                'name',
                'surname',
                'avatar',
                'status'
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthorPost()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id'])
            ->from(['author_post_comment' => 'prm_user'])
            ->select([
                'id',
                'name',
                'surname',
                'avatar',
                'status'
            ]);
    }

    public function getCommentPost()
    {
        return $this->hasMany(CommentPromise::className(), ['post_id' => 'id'])
            ->from(['comment_to_post' => 'prm_comment_promise'])
            ->onCondition('comment_to_post.post_id IS NOT NULL');
    }

    /**
     * @inheritdoc
     * @return CommentPromiseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommentPromiseQuery(get_called_class());
    }
}
