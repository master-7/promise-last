<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_promise_rate".
 *
 * @property integer $promise_id
 * @property integer $type_rate
 * @property string $rate
 *
 * @property Promise $promise
 * @property PromiseRateType $typeRate
 */
class PromiseRate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_promise_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promise_id', 'type_rate', 'rate'], 'required'],
            [['promise_id', 'type_rate'], 'integer'],
            [['rate'], 'string'],
            [['promise_id'], 'exist',
                'targetClass' => Promise::className(), 'targetAttribute' => ['promise_id' => 'id']],
            [['type_rate'], 'exist',
                'targetClass' => PromiseRateType::className(), 'targetAttribute' => ['type_rate' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'promise_id' => Yii::t('app', 'Promise ID'),
            'type_rate' => Yii::t('app', 'Type Rate'),
            'rate' => Yii::t('app', 'Rate'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromise()
    {
        return $this->hasOne(Promise::className(), ['id' => 'promise_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeRate()
    {
        return $this->hasOne(PromiseRateType::className(), ['id' => 'type_rate']);
    }

    /**
     * @inheritdoc
     * @return PromiseRateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PromiseRateQuery(get_called_class());
    }
}
