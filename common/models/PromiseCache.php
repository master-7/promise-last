<?php

namespace common\models;

use Yii;
use Yii\helpers\ArrayHelper;

/**
 * This is the model class for table "prm_promise_cache".
 *
 * @property integer $promise_id
 * @property integer $user_id
 * @property string $json_data
 *
 * @property Promise $promise
 * @property User $user
 */
class PromiseCache extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_promise_cache';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promise_id', 'json_data'], 'required'],
            [['promise_id'], 'integer'],
            [['user_id'], 'integer'],
            [['json_data'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'promise_id' => Yii::t('app', 'Promise ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'json_data' => Yii::t('app', 'Json Data'),
        ];
    }

    /**
     * Save the promise cache
     * @param $promise_id
     */
    public static function doPromiseCache($promise_id)
    {
        $news = Promise::find()
            ->joinWith([
                'promiseWork',
                'assessmentComment',
                'assessmentComment.author',
                'assessmentComment.commentPost',
                'assessmentComment.commentPost.authorPost'
            ])
            ->withPk($promise_id)
            ->asArray()
            ->one();

        foreach ($news['assessmentComment'] as $key => $val) {
            $news['assessmentComment'][$key]['commentPost'] =
                ArrayHelper::index($val['commentPost'], function ($element) {
                    return $element['id'];
                });
        }

        $promiseCache = self::findOne($promise_id);

        if(!$promiseCache)
        {
            $promiseCache = new self();
        }
        $promiseCache->promise_id = $promise_id;
        $promiseCache->user_id = $news['user_id'];
        $promiseCache->json_data = json_encode($news);
        $promiseCache->save();
    }

    /**
     * The test method! Not do in production
     */
    public static function doAllPromiseCache()
    {
        /**
         * @todo Drop this method before upload in production
         */

        $news = Promise::find()
            ->joinWith([
                'promiseWork',
                'assessmentComment',
                'assessmentComment.author',
                'assessmentComment.commentPost',
                'assessmentComment.commentPost.authorPost'
            ])
            ->asArray()
            ->all();

        foreach($news as $key => $val) {

            foreach ($val['assessmentComment'] as $k => $v) {
                $val['assessmentComment'][$k]['commentPost'] =
                    ArrayHelper::index($v['commentPost'], function ($element) {
                        return $element['id'];
                    });
            }

            $commentCache = self::findOne($val['id']);

            if (!$commentCache) {
                $commentCache = new self();
            }
            $commentCache->promise_id = $val['id'];
            $commentCache->user_id = $val['user_id'];
            $commentCache->json_data = json_encode($val);
            $commentCache->save();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromise()
    {
        return $this->hasOne(Promise::className(), ['id' => 'promise_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return PromiseCacheQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PromiseCacheQuery(get_called_class());
    }
}
