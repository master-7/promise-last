<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_promise".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $category_id
 * @property string $user_surname
 * @property string $user_name
 * @property string $image_path
 * @property integer $rate
 * @property string $title
 * @property string $short_text
 * @property string $description
 * @property string $success
 * @property integer $views
 * @property string $create_date
 * @property string $deadline
 *
 * @property User $user
 */
class Promise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_promise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'user_surname', 'user_name', 'image_path',
                'rate', 'title', 'short_text', 'description', 'deadline'], 'required'],
            [['user_id', 'rate', 'views'], 'integer'],
            [['description'], 'string'],
            [['category_id'], 'exist',
                'targetClass' => Category::className(),
                'targetAttribute' => 'id'],
            [['create_date', 'deadline'], 'safe'],
            [['user_surname', 'user_name', 'title'], 'string', 'max' => 50],
            [['image_path', 'success'], 'string', 'max' => 45],
            [['short_text'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'user_surname' => Yii::t('app', 'User Surname'),
            'user_name' => Yii::t('app', 'User Name'),
            'image_path' => Yii::t('app', 'Image Path'),
            'rate' => Yii::t('app', 'Rate'),
            'title' => Yii::t('app', 'Title'),
            'short_text' => Yii::t('app', 'Short Text'),
            'description' => Yii::t('app', 'Description'),
            'success' => Yii::t('app', 'Success'),
            'views' => Yii::t('app', 'Views'),
            'create_date' => Yii::t('app', 'Create Date'),
            'deadline' => Yii::t('app', 'Deadline'),
        ];
    }

    /**
     * After save recount rating and cache promise
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        Promise::recountRating($this->id);

        PromiseCache::doPromiseCache($this->id);

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * This function recount rating promise
     * @param $promise_id
     */
    public static function recountRating($promise_id)
    {
        //Get rating for stat query
        $rating = PromiseAssessment::find()->
        getPromiseRating()->
        withPromise($promise_id)->
        asArray()->
        one();
        //If rating write promise model new rating value
        if(isset($rating["rating"])) {
            $promise = self::findOne($promise_id);
            $promise->success = (int)$rating["rating"];
            $promise->save();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessmentComment()
    {
        return $this->hasMany(CommentPromise::className(), ['news_id' => 'id'])
            ->from(['assessment_comment' => 'prm_comment_promise'])
            ->select([
                'id',
                'news_id',
                'author_id',
                'comment',
                'assessment',
                'positive',
                'negative',
                'create_date'
            ])
            ->onCondition('assessment_comment.post_id IS NULL');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromiseWork()
    {
        return $this->hasMany(PromiseWork::className(), ['promise_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasMany(Category::className(), ['category_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return PromiseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PromiseQuery(get_called_class());
    }
}
