<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_promise_work_comment".
 *
 * @property integer $id
 * @property integer $promise_work_id
 * @property integer $author_id
 * @property integer $author_hidden
 * @property string $comment
 * @property integer $positive
 * @property integer $negative
 * @property integer $status
 * @property string $create_date
 *
 * @property PromiseWork $promiseWork
 * @property User $author
 */
class PromiseWorkComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_promise_work_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promise_work_id', 'author_id', 'author_hidden', 'comment', 'positive', 'negative', 'create_date'], 'required'],
            [['promise_work_id', 'author_id', 'author_hidden', 'positive', 'negative', 'status'], 'integer'],
            [['comment'], 'string'],
            [['create_date'], 'safe'],
            [['promise_work_id'], 'exist',
                'targetClass' => PromiseWork::className(), 'targetAttribute' => ['promise_work_id' => 'id']],
            [['author_id'], 'exist',
                'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'promise_work_id' => Yii::t('app', 'Promise Work ID'),
            'author_id' => Yii::t('app', 'Author ID'),
            'author_hidden' => Yii::t('app', 'Author Hidden'),
            'comment' => Yii::t('app', 'Comment'),
            'positive' => Yii::t('app', 'Positive'),
            'negative' => Yii::t('app', 'Negative'),
            'status' => Yii::t('app', 'Status'),
            'create_date' => Yii::t('app', 'Create Date'),
        ];
    }

    /**
     * After save recount cache comments
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        PromiseWorkCache::doPromiseWorkCache($this->promise_work_id);

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromiseWork()
    {
        return $this->hasOne(PromiseWork::className(), ['id' => 'promise_work_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @inheritdoc
     * @return PromiseWorkCommentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PromiseWorkCommentQuery(get_called_class());
    }
}
