<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_promise_rate_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property PromiseRate[] $prmPromiseRates
 */
class PromiseRateType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_promise_rate_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromiseRates()
    {
        return $this->hasMany(PromiseRate::className(), ['type_rate' => 'id']);
    }
}
