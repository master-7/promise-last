<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PromiseCache]].
 *
 * @see PromiseCache
 */
class PromiseCacheQuery extends \yii\db\ActiveQuery
{
    /**
     * Add condition with user id
     * @param $user_id
     * @return $this
     */
    public function withUser($user_id)
    {
        $this->andWhere(
            'prm_promise_cache.user_id = :user_id',
            [
                ':user_id' => $user_id
            ]
        );
        return $this;
    }
    /**
     * Add condition with promise id
     * @param $promise_id
     * @return $this
     */
    public function withPromise($promise_id)
    {
        $this->andWhere(
            'prm_promise_cache.promise_id = :promise_id',
            [
                ':promise_id' => $promise_id
            ]
        );
        return $this;
    }

    /**
     * @inheritdoc
     * @return PromiseWork[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PromiseWork|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}