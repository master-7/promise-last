<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_promise_work_comment_assessment".
 *
 * @property integer $work_comment_id
 * @property integer $user_id
 * @property integer $assessment
 *
 * @property PromiseWorkComment $workComment
 * @property User $user
 */
class PromiseWorkCommentAssessment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_promise_work_comment_assessment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['work_comment_id', 'user_id', 'assessment'], 'required'],
            [['work_comment_id', 'user_id', 'assessment'], 'integer'],
            [['work_comment_id'], 'exist',
                'targetClass' => PromiseWorkComment::className(), 'targetAttribute' => ['work_comment_id' => 'id']],
            [['user_id'], 'exist',
                'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'work_comment_id' => Yii::t('app', 'Work Comment ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'assessment' => Yii::t('app', 'Assessment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkComment()
    {
        return $this->hasOne(PromiseWorkComment::className(), ['id' => 'work_comment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return PromiseWorkCommentAssessmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PromiseWorkCommentAssessmentQuery(get_called_class());
    }
}
