<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_comment_promise_assessment".
 *
 * @property integer $comment_promise
 * @property integer $user_id
 * @property integer $assessment
 * @property string $create_date
 *
 * @property CommentPromise $commentPromise
 * @property User $user
 */
class CommentPromiseAssessment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_comment_promise_assessment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment_promise', 'user_id', 'assessment'], 'required'],
            [['comment_promise', 'user_id', 'assessment'], 'integer'],
            [['create_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comment_promise' => Yii::t('app', 'Comment Promise'),
            'user_id' => Yii::t('app', 'User ID'),
            'assessment' => Yii::t('app', 'Assessment'),
            'create_date' => Yii::t('app', 'Create Date'),
        ];
    }

    /**
     * Before save need add assessment to dependents model and recount rating to
     * promise
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if($this->getIsNewRecord()) {
            /**
             * Check comments for the condition
             */
            $commentPromise = CommentPromise::findOne($this->comment_promise);

            if($this->assessment > 0) {
                $commentPromise->positive = $this->assessment;
            } else {
                $commentPromise->negative = $this->assessment;
            }
            $commentPromise->save();

            $commentAssessment = self::find()
                ->getCommentRating()
                ->withCommentPromise($commentPromise->id)
                ->asArray()
                ->one();

            $promiseCommentRatingNegativeAndStatusActive =
                ((abs($commentAssessment['rating']) + $commentPromise->positive) - $commentPromise->negative) <= 0
                    &&
                $commentPromise->status == CommentPromise::STATUS_ACTIVE;

            $promiseCommentRatingPositiveAndStatusNotActive =
                ((abs($commentAssessment['rating']) + $commentPromise->positive) - $commentPromise->negative) > 0
                    &&
                $commentPromise->status == CommentPromise::STATUS_NOT_ACTIVE;

            if($promiseCommentRatingNegativeAndStatusActive) {
                PromiseAssessment::assessmentsOnOff($commentPromise->id, PromiseAssessment::STATUS_NOT_ACTIVE);
                $commentPromise->status = CommentPromise::STATUS_NOT_ACTIVE;
                $commentPromise->save();
            } elseif($promiseCommentRatingPositiveAndStatusNotActive) {
                PromiseAssessment::assessmentsOnOff($commentPromise->id, PromiseAssessment::STATUS_ACTIVE);
            }

            $user = User::findOne($commentPromise->author_id);
            $user->assessment = $user->assessment + $this->assessment;
            $user->save();
        }
        return parent::beforeSave($insert);
    }

    /**
     * After save recount rating promise
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $commentPromise = CommentPromise::findOne($this->comment_promise);

        Promise::recountRating($commentPromise->news_id);

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommentPromise()
    {
        return $this->hasOne(CommentPromise::className(), ['id' => 'comment_promise']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return CommentPromiseAssessmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommentPromiseAssessmentQuery(get_called_class());
    }
}
