<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_multimedia_resource".
 *
 * @property integer $id
 * @property integer $gallery_id
 * @property string $link
 * @property string $description
 * @property integer $type
 *
 * @property MultimediaGallery $gallery
 */
class MultimediaResource extends \yii\db\ActiveRecord
{
    /**
     * Validate constant
     */
    const MAX_LENGTH_LINK_AND_DESCRIPTION = 255;
    /**
     * Multimedia type
     */
    const TYPE_PHOTO = 1;
    const TYPE_VIDEO = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_multimedia_resource';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gallery_id', 'link', 'type'], 'required'],
            [['id', 'gallery_id', 'type'], 'integer'],
            [['link', 'description'], 'string', 'max' => self::MAX_LENGTH_LINK_AND_DESCRIPTION],
            [['gallery_id'], 'exist',
                'targetClass' => MultimediaGallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'gallery_id' => Yii::t('app', 'Gallery ID'),
            'link' => Yii::t('app', 'Link'),
            'description' => Yii::t('app', 'Description'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasOne(MultimediaGallery::className(), ['id' => 'gallery_id']);
    }

    /**
     * @inheritdoc
     * @return MultimediaResourceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MultimediaResourceQuery(get_called_class());
    }
}
