<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PromiseWorkCache]].
 *
 * @see PromiseWorkCache
 */
class PromiseWorkCacheQuery extends \yii\db\ActiveQuery
{
    /**
     * Add condition with id
     * @param $id
     * @return $this
     */
    public function withPk($id)
    {
        $this->andWhere(
            'prm_promise_work_cache.id = :id',
            [
                ':id' => $id
            ]
        );
        return $this;
    }

    /**
     * @inheritdoc
     * @return PromiseWorkCache[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PromiseWorkCache|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}