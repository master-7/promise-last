<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_complaint".
 *
 * @property integer $id
 * @property integer $promise_id
 * @property integer $type
 *
 * @property Promise $promise
 * @property ComplaintType $complaintType
 */
class Complaint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_complaint';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promise_id', 'type'], 'required'],
            [['promise_id', 'type'], 'integer'],
            [['promise_id'], 'exist',
                'targetClass' => Promise::className(), 'targetAttribute' => ['promise_id' => 'id']],
            [['type'], 'exist',
                'targetClass' => ComplaintType::className(), 'targetAttribute' => ['type' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'promise_id' => Yii::t('app', 'Promise ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromise()
    {
        return $this->hasOne(Promise::className(), ['id' => 'promise_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplaintType()
    {
        return $this->hasOne(ComplaintType::className(), ['id' => 'type']);
    }
}
