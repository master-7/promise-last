<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prm_promise_work_cache".
 *
 * @property integer $promise_id
 * @property integer $work_id
 * @property integer $user_id
 * @property string $json_data
 *
 * @property User $user
 * @property Promise $promise
 * @property PromiseWork $work
 */
class PromiseWorkCache extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prm_promise_work_cache';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promise_id', 'work_id'], 'required'],
            [['promise_id', 'work_id', 'user_id'], 'integer'],
            [['json_data'], 'string'],
            [['user_id'], 'exist',
                'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['promise_id'], 'exist',
                'targetClass' => Promise::className(), 'targetAttribute' => ['promise_id' => 'id']],
            [['work_id'], 'exist',
                'targetClass' => PromiseWork::className(), 'targetAttribute' => ['work_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'promise_id' => Yii::t('app', 'Promise ID'),
            'work_id' => Yii::t('app', 'Work ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'json_data' => Yii::t('app', 'Json Data'),
        ];
    }

    /**
     * Save the promise work cache
     * @param $promise_work_id
     */
    public static function doPromiseWorkCache($promise_work_id)
    {
        $promiseWork = PromiseWork::find()
            ->joinWith([
                'multimediaResource',
                'promiseWorkComments',
                'promiseWorkComments.author'
            ])
            ->withPk($promise_work_id)
            ->withUser(Yii::$app->user->id)
            ->asArray()
            ->one();

        $workPromiseCache = null;

        if($promiseWork) {
            $workPromiseCache = self::findOne([
                'work_id' => $promiseWork["id"],
                'promise_id' => $promiseWork["promise_id"],
                'user_id' => $promiseWork["user_id"]
            ]);

            if (!$workPromiseCache) {
                $workPromiseCache = new self();
            }
            $workPromiseCache->promise_id = $promiseWork["promise_id"];
            $workPromiseCache->work_id = $promiseWork["id"];
            $workPromiseCache->user_id = $promiseWork["user_id"];
            $workPromiseCache->json_data = json_encode($promiseWork);
            $workPromiseCache->save();
        }
    }

    /**
     * The test method! Not do in production
     */
    public static function doAllPromiseWorkCache()
    {
        /**
         * @todo Drop this method before upload in production
         */

        $promiseWork = PromiseWork::find()
            ->joinWith([
                'multimediaResource',
                'promiseWorkComments',
                'promiseWorkComments.author'
            ])
            ->asArray()
            ->all();

        foreach($promiseWork as $key => $val) {

            $workPromiseCache = self::findOne([
                'work_id' => $val["id"],
                'promise_id' => $val["promise_id"],
                'user_id' => $val["user_id"]
            ]);

            if(!$workPromiseCache)
            {
                $workPromiseCache = new self();
            }
            $workPromiseCache->promise_id = $val["promise_id"];
            $workPromiseCache->work_id = $val["id"];
            $workPromiseCache->user_id = $val["user_id"];
            $workPromiseCache->json_data = json_encode($val);
            $workPromiseCache->save();
        }
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromise()
    {
        return $this->hasOne(Promise::className(), ['id' => 'promise_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWork()
    {
        return $this->hasOne(PromiseWork::className(), ['id' => 'work_id']);
    }

    /**
     * @inheritdoc
     * @return PromiseWorkCacheQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PromiseWorkCacheQuery(get_called_class());
    }
}
