<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[MultimediaResource]].
 *
 * @see MultimediaResource
 */
class MultimediaResourceQuery extends \yii\db\ActiveQuery
{
    /**
     * Add condition with gallery
     * @param $gallery_id
     * @return $this
     */
    public function withGallery($gallery_id)
    {
        $this->andWhere(
            'prm_multimedia_resource.gallery_id = :gallery_id',
            [
                ':gallery_id' => $gallery_id
            ]
        );
        return $this;
    }

    /**
     * @inheritdoc
     * @return MultimediaResource[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MultimediaResource|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}