<?php

namespace common\helpers;

use yii;

class SaveImageHelper {

    /**
     * String getting the function file_get_content()
     * @var
     */
    private $avatar;
    /**
     * Link to avatar
     * @var
     */
    private $avatarLink;
    /**
     * Base directory name
     * @var
     */
    private $directoryName;
    /**
     * Where save path
     * @var
     */
    private $savePath;

    /**
     * Base constructor
     * @param $avatar - avatar string getting the function file_get_content()
     * @param $avatarLink - link to avatar
     */
    public function __construct($avatar, $avatarLink = null)
    {
        $this->avatar = $avatar;
        $this->avatarLink = $avatarLink;
    }

    /**
     * Function save avatar
     * @param null $extension
     * @return string
     * @throws yii\base\ErrorException
     */
    public function saveAvatar($extension = null)
    {
        $this->savePath = Yii::getAlias('@webroot/images/avatar');

        return $this->saveImage($extension);
    }

    /**
     * Function save promise image
     * @param null $extension
     * @return string
     * @throws yii\base\ErrorException
     */
    public function savePromiseImage($extension = null)
    {
        $this->savePath = Yii::getAlias('@webroot/images/promises');

        return $this->saveImage($extension);
    }

    /**
     * Function save promise work image
     * @param null $extension
     * @return string
     * @throws yii\base\ErrorException
     */
    public function savePromiseWorkImage($extension = null)
    {
        $this->savePath = Yii::getAlias('@webroot/images/promises_work');

        return $this->saveImage($extension);
    }

    /**
     * Save image common method
     * @param $extension
     * @return string
     * @throws yii\base\ErrorException
     */
    protected function saveImage($extension)
    {
        $this->generateDirForCurrentDate();

        if(!$extension) {
            if(!$this->avatarLink)
                throw new yii\base\ErrorException('Point imageLink or file extension!');
            $size = getimagesize($this->avatarLink);
            $extension = image_type_to_extension($size[2]);
        }

        $name = DIRECTORY_SEPARATOR . $this->generateUniqueFileName($extension);

        $avatarFullName = $this->savePath . $name;

        file_put_contents($avatarFullName, $this->avatar);

        return $this->directoryName . $name;
    }

    /**
     * Generate dir with name equals current date
     * @return string
     */
    public function generateDirForCurrentDate()
    {
        $date = new \DateTime();
        $this->directoryName = $date->format('Y-m');
        $pathToFolderAvatar = $this->savePath . DIRECTORY_SEPARATOR . $this->directoryName;
        if(!is_dir($pathToFolderAvatar))
        {
            mkdir($pathToFolderAvatar);
        }
        $this->savePath = $pathToFolderAvatar;
    }

    /**
     * Generate unique file name
     * @param $fileExtension
     * @return string
     */
    public function generateUniqueFileName($fileExtension)
    {
        $avatarName = hash("md5", uniqid('token_', true)) . $fileExtension;
        while(file_exists($this->savePath . DIRECTORY_SEPARATOR . $avatarName))
        {
            $avatarName = hash("md5", uniqid('token_', true)) . $fileExtension;
        }

        return $avatarName;
    }

    /**
     * Function delete avatar by name
     * @param $fileName
     * @return bool
     */
    public static function deleteAvatar($fileName)
    {
        $filePath = Yii::getAlias('@webroot/images/avatar') . DIRECTORY_SEPARATOR . $fileName;
        if(file_exists($filePath))
            return unlink($filePath);
    }

    /**
     * Function delete images by name
     * @param $fileName
     * @return bool
     */
    public static function deleteImage($fileName)
    {
        $filePath = Yii::getAlias('@webroot/images/promises') . DIRECTORY_SEPARATOR . $fileName;
        if(file_exists($filePath))
            return unlink($filePath);
    }

    /**
     * Get type by base64 string
     * @param $base64string
     * @return null
     */
    public static function getTypeByBase64($base64string)
    {
        preg_match('/data:image\/(?P<type>\w+);base64,/', $base64string, $type);
        if(isset($type['type']))
            return $type['type'];
        return null;
    }

    /**
     * Get clear base 64 string
     * @param $base64string
     * @return mixed
     */
    public static function clearBase64string($base64string)
    {
        return preg_replace('/data:image\/\w+;base64,/', '', $base64string);
    }

}