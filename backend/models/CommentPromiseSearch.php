<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CommentPromise;

/**
 * CommentPromiseSearch represents the model behind the search form about `common\models\CommentPromise`.
 */
class CommentPromiseSearch extends CommentPromise
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'news_id', 'author_id', 'author_hidden', 'assessment', 'positive', 'negative'], 'integer'],
            [['comment', 'create_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CommentPromise::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'news_id' => $this->news_id,
            'author_id' => $this->author_id,
            'author_hidden' => $this->author_hidden,
            'assessment' => $this->assessment,
            'positive' => $this->positive,
            'negative' => $this->negative,
            'create_date' => $this->create_date,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
