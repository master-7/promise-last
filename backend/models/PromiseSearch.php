<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Promise;

/**
 * PromiseSearch represents the model behind the search form about `common\models\Promise`.
 */
class PromiseSearch extends Promise
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'rate', 'views'], 'integer'],
            [['user_surname', 'user_name', 'image_path', 'title', 'short_text', 'description', 'success', 'create_date', 'start_date', 'deadline'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Promise::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'rate' => $this->rate,
            'views' => $this->views,
            'create_date' => $this->create_date,
            'start_date' => $this->start_date,
            'deadline' => $this->deadline,
        ]);

        $query->andFilterWhere(['like', 'user_surname', $this->user_surname])
            ->andFilterWhere(['like', 'user_name', $this->user_name])
            ->andFilterWhere(['like', 'image_path', $this->image_path])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'short_text', $this->short_text])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'success', $this->success]);

        return $dataProvider;
    }
}
