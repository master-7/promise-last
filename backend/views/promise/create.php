<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Promise */

$this->title = Yii::t('app', 'Create Promise');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Promises'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promise-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
