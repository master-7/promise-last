<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CommentPromise */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Comment Promise',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Comment Promises'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="comment-promise-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
