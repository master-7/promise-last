<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CommentPromise */

$this->title = Yii::t('app', 'Create Comment Promise');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Comment Promises'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-promise-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
